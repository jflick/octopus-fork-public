#!/usr/bin/python                                                                                                                                             \
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata

np.set_printoptions(threshold=np.nan)

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 30,
        }

mpl.rcParams['xtick.labelsize'] = 30
mpl.rcParams['ytick.labelsize'] = 30

mpl.rc('font', family='serif')
mpl.rc('font', serif='Times')
#mpl.rc('text', usetex='true')
mpl.rcParams.update({'font.size': 30})

phot=1 # add photon q to the plot

dynamics_dir = 'td.general/coordinates'
data = np.loadtxt('td.general/coordinates', skiprows=5)
#dataphotonq = np.loadtxt('td.general/photons_q', skiprows=5)[:,2]
forces_data = data[:,2+27*3*2:]
fF = forces_data[:,0:3]
fSi= forces_data[:,3:6]
fC = forces_data[:,6:9]
fC_CC = forces_data[:,15*3:16*3]
fC_CBenz = forces_data[:,16*3:17*3]

fSiF = fSi-fF
fSiC = fSi-fC
fCC = fC-fC_CC
fCCB = fC_CC-fC_CBenz

time = data[:,1]*0.02418884254*10
nframes = int(len(data[:,0])*0.01)
bcor = np.zeros((nframes,5))

count = 0
for frame in range(nframes):
    bcor[count,0] = time[frame*100]
    bcor[count,1] = abs(np.dot(fSiF[frame*100,:],fSiC[frame*100,:]))
    bcor[count,2] = abs(np.dot(fSiC[frame*100,:],fCC[frame*100,:]))
    bcor[count,3] = abs(np.dot(fCC[frame*100,:],fCCB[frame*100,:]))
    bcor[count,4] = abs(np.dot(fSiC[frame*100,:],fCCB[frame*100,:]))
    count+=1
np.savetxt('frames/bond_correlation.txt', bcor)

bcor_int = np.sum(bcor,axis=0)/count
np.savetxt('frames/bond_correlation_int.txt', bcor_int)
