# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from scipy.interpolate import make_interp_spline
from matplotlib.path import Path
import matplotlib.patches as patches
import matplotlib
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D

from pylab import title as pytitle
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}
matplotlib.rcParams['xtick.labelsize'] = 8
matplotlib.rcParams['ytick.labelsize'] = 8

matplotlib.rc('font', **font)

abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale
outfile='td_distances_frequency_scan.pdf'

def main():

    fileslist = [2,4,8,10,5,12,18,19] #fileslist_enrico = [2,4,8,10]
    files = len(fileslist)
    dirlist = ['300K_cavity_0.001950111494317', '300K_cavity_0.002600148659089', '300K_cavity_0.003120178390907', '300K_cavity', '300K_cavity_0.004875278735793', '300K_cavity_0.00585033448', '300K_cavity_0.007800445977268', 
               '300K_cavity_0.0009750557472', '300K_cavity_0.001462583621', '300K_cavity_0.0001950111494', '300K_cavity_0.0002600148659', '300K_cavity_0.0003120178391', '300K_cavity_0.0003900222989', '300K_cavity_0.0004875278736', 
               '300K_cavity_0.0005850334483', '300K_cavity_0.0007800445977', '300K_cavity_0.001696597', '300K_cavity_0.002242628218', '300K_cavity_0.00351020069', '300K_cavity_0.004212240828', '300K_cavity_0.004485256437', 
               '300K_cavity_0.001209069126', '300K_cavity_0.002086619299', '300K_cavity_0.005265301035', '300K_cavity_0.006435367931', '300K_cavity_0.00682539023', '300K_cavity_0.007215412529', '../../F_opposite/300K']
    #, '300K_cavity_0.002600148659089/05_couplinghalf'
    dirlist_enrico = ['0.5_omega','0.66_omega','0.8_omega','omega','1.25_omega','1.5_omega','2_omega','0.25_omega','0.375_omega','0.05_omega','0.066_omega','0.08_omega','0.1_omega','0.125_omega','0.15_omega','0.2_omega',
                      '0.435_omega', '0.575_omega', '0.9_omega', '1.08_omega', '1.15_omega', '0.31_omega', '0.535_omega', '1.35_omega', '1.65_omega', '1.75_omega', '1.85_omega', '../../F_opposite/300K']
    #,'0.66_omega/0.5_lambda'
    lenmax = 888 #1100 #888
    lenstart_short = 633
    cutplots = 7
    data = np.zeros((len(dirlist),lenmax))
    tr_avdis = np.zeros((len(dirlist),files))
    tr_avvar = np.zeros((len(dirlist),files))
    tr_avdis_short = np.zeros((len(dirlist),files))
    datacontainer = np.zeros(lenmax)
    counter = -1
    for dirs in dirlist:
      counter +=1
      counterin = -1
      for jj in fileslist:
        counterin +=1
        if dirs != '../../F_opposite/300K' and dirs != '300K_cavity_0.002600148659089/05_couplinghalf':
          if jj == 5:
            print('../'+dirs+'/'+'{:02d}'.format(jj))
            dummy = np.loadtxt('../'+dirs+'/'+'{:02d}'.format(jj)+'/frames/bond_length.dat')
          elif (jj < 11) and jj!=5:
            print('enrico '+dirlist_enrico[counter]+'{:02d}'.format(jj))
            dummy = np.loadtxt('enrico/'+'{:02d}'.format(jj)+'/'+dirlist_enrico[counter]+'/bond_length.dat')
          elif (jj > 10):
            print('{:02d}'.format(jj)+'-'+str(counter+1))
            dummy = np.loadtxt('johannes/bond_length-'+'{:02d}'.format(jj)+'-'+str(counter+1)+'.dat')
        elif dirs == '../../F_opposite/300K':
          if jj == 5:
            print('../'+dirs+'/'+'{:02d}'.format(jj))
            dummy = np.loadtxt(dirs+'/'+'{:02d}'.format(jj)+'/frames/bond_length.dat')
          elif (jj < 11) and jj!=5:
            print('../'+dirs+'/'+'{:02d}'.format(jj+10))
            dummy = np.loadtxt(dirs+'/'+'{:02d}'.format(jj+10)+'/frames/bond_length.dat')
          elif (jj > 10):
            print('../'+dirs+'/'+'{:02d}'.format(jj+10))
            dummy = np.loadtxt(dirs+'/'+'{:02d}'.format(jj+10)+'/frames/bond_length.dat')
        elif dirs == '300K_cavity_0.002600148659089/05_couplinghalf':
          if jj == 5:
            print('../'+dirs+'/'+'{:02d}'.format(jj))
            dummy = np.loadtxt('../'+dirs+'/frames/bond_length.dat')
          elif (jj < 11) and jj!=5:
            print('enrico '+dirlist_enrico[counter]+'{:02d}'.format(jj))
            dummy = np.loadtxt('enrico/'+'{:02d}'.format(jj)+'/'+dirlist_enrico[counter]+'/bond_length.dat')
          elif (jj > 10):
            print('{:02d}'.format(jj)+'-'+str(28))
            dummy = np.loadtxt('johannes/bond_length-'+'{:02d}'.format(jj)+'-'+str(28)+'.dat')

        datacontainer = np.zeros(lenmax)
        datacontainer = dummy[:lenmax,1]*abohr
        data[counter,:] = data[counter,:] + datacontainer/float(len(fileslist))
        tr_avdis[counter,counterin] = np.sum(datacontainer)/lenmax
        tr_avvar[counter,counterin] = np.sum(np.square(datacontainer))/lenmax - (np.sum(datacontainer)/lenmax)**2
        tr_avdis_short[counter,counterin] = np.sum(datacontainer[lenstart_short:])/(lenmax-lenstart_short)
        if counter==0:
          time = dummy[:lenmax,0]
    print(time[lenstart_short])
    print(time[-1])

    print('Spectrum from directory: /home/schaferc/projects/transitionstates/vibrational_eigenmodes_analysis/interpolation')
    spectrumx = np.loadtxt('spectrumx.dat')

##############################
    XX = np.array([0.001950111494317,0.002600148659089,0.003120178390907,0.003900222988634,0.004875278735793,0.00585033448,0.007800445977268,0.0009750557472,0.001462583621,0.0001950111494,0.0002600148659,0.0003120178391,
                   0.0003900222989,0.0004875278736,0.0005850334483,0.0007800445977, 0.001696597, 0.002242628218, 0.00351020069, 0.004212240828, 0.004485256437, 0.001209069126, 0.002086619299, 0.005265301035, 0.006435367931, 0.00682539023, 0.007215412529, 0])*219474.63068
    #, 0.002600148659089
    #average_dis = np.zeros(7)
    #average_var = np.zeros(7)

    #fig, (ax0, ax3, ax4) = plt.subplots(nrows=1,ncols=3,sharex=True)
    gs = gridspec.GridSpec(3,2)
    gs.update(left=0.05, right=0.95, wspace=0.3, hspace=0.25)
    pl.figure(figsize=(6, 10))

    #cmap = matplotlib.cm.get_cmap('coolwarm_r')
    cmap = matplotlib.cm.get_cmap('coolwarm_r')
    ax0 = pl.subplot(gs[0, :]) # row 0, col 0
    for jj in range(cutplots):
      if jj==3:
        ax0.plot(time, data[jj,:],color='orange',linewidth=2,linestyle='--',label=r''+'{:.2f}'.format(XX[jj]/XX[3])+'$\omega_r$' ) # str(XX[jj]))
      else:
        ax0.plot(time, data[jj,:],color=cmap(jj*1/(cutplots-1)),markersize=1,label=r''+'{:.2f}'.format(XX[jj]/XX[3])+'$\omega_r$' ) # str(XX[jj]))
    ax0.plot(time, data[27,:],'k:',label=r'$g_0=0$' ) # entry 27 should be the free-space solution which is loaded from the F_opposite directory
    #ax0.plot(time, data[28,:],'g--',label=r'$g_0/\hbar\omega_c=0.566,~\omega_c=571/cm$' ) # entry 28 is the half-coupling run at 570/cm
      #average_dis[jj] = np.sum(data[jj,:])/lenmax
      #average_var[jj] = np.sum(np.square(data[jj,:]))/lenmax - (np.sum(data[jj,:])/lenmax)**2
    #ax0.grid(True)
    ax0.set_xlim(0,700) #868)
    ax0.legend(frameon=False)
    ax0.set(xlabel=r'Time (fs)', ylabel=r'$\langle R_{SiC}(t) \rangle_{tr}$ ($\mathring{A}$)')
    ax0.text(145,2.77, r'(a)', fontsize=14) 
    plt.arrow(500, 1.85, 0, +0.05, width=0.1, head_width = 5, head_length = 0.05, color='black')

    #ax1 = pl.subplot(gs[1, :]) # row 0, col 0
    #for jj in range(cutplots,16):
    #  if jj==12:
    #    ax1.plot(time, data[jj,:],color='orange',linewidth=2,linestyle='--',label=r''+'{:.2f}'.format(XX[jj]/XX[3])+'$\omega_r$' ) # str(XX[jj]))
    #  else:
    #    ax1.plot(time, data[jj,:],color=cmap((jj-9)*1/(16-cutplots)),markersize=1,label=r''+'{:.2f}'.format(XX[jj]/XX[3])+'$\omega_r$' ) # str(XX[jj]))
    ##ax0.plot(time, data[7,:],'k:',label=r'$g_0=0$' ) # str(XX[jj]))
    #  #average_dis[jj] = np.sum(data[jj,:])/lenmax
    #  #average_var[jj] = np.sum(np.square(data[jj,:]))/lenmax - (np.sum(data[jj,:])/lenmax)**2
    ##ax0.grid(True)
    #ax1.set_xlim(0,700) #868)
    #ax1.legend(frameon=False, loc='upper left')
    #ax1.set(xlabel=r'Time (fs)', ylabel=r'$\langle R_{SiC}(t) \rangle_{tr}$ ($\mathring{A}$)')
    #ax1.text(145,2.77, r'(b)', fontsize=14) 
    #plt.arrow(500, 1.85, 0, +0.05, width=0.1, head_width = 5, head_length = 0.05, color='black')

    tr_avdis_std = np.zeros(len(dirlist))
    tr_avvar_std = np.zeros(len(dirlist))
    avdis = np.zeros(len(dirlist))
    avvar = np.zeros(len(dirlist))

    tr_avdis_std_short = np.zeros(len(dirlist))
    avdis_short = np.zeros(len(dirlist))

    for jj in range(len(dirlist)):
      avdis[jj] = np.sum(tr_avdis[jj,:])/float(files)
      avvar[jj] = np.sum(tr_avvar[jj,:])/float(files)
      tr_avdis_std[jj] = np.sqrt( np.sum(np.square(tr_avdis[jj,:]))/float(files) - (np.sum(tr_avdis[jj,:])/float(files))**2 )/np.sqrt(float(files))
      tr_avvar_std[jj] = np.sqrt( np.sum(np.square(tr_avvar[jj,:]))/float(files) - (np.sum(tr_avvar[jj,:])/float(files))**2 )/np.sqrt(float(files))

      avdis_short[jj] = np.sum(tr_avdis_short[jj,:])/float(files)
      tr_avdis_std_short[jj] = np.sqrt( np.sum(np.square(tr_avdis_short[jj,:]))/float(files) - (np.sum(tr_avdis_short[jj,:])/float(files))**2 )/np.sqrt(float(files))

    #ax3 = pl.subplot(gs[2, 0]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    #ax3.plot(XX[:cutplots], avdis[:cutplots],'ko',markersize=2)
    #ax3.errorbar(XX[:cutplots],avdis[:cutplots], yerr=tr_avdis_std[:cutplots], fmt='o',color='black')
    #ax3.set(xlabel=r'$\omega_c$ (cm$^{-1}$)', ylabel=r'$ \langle\langle R_{SiC}\rangle\rangle_{tr}$ ($\mathring{A}$)')
    #plt.locator_params(axis='x', nbins=10)
    #plt.locator_params(axis='y', nbins=4)
    #plt.axvline(x=856, color=cmap(3*1/7), linestyle='--')
    #plt.axvline(x=856, color='orange', linestyle='--')
    #plt.axhline(y=avdis[7], color='black', linestyle=':')
    #ax3.text(1400,avdis[7]+0.005, r'$g_0=0$', fontsize=10) 
    #ax3.text(880,2.1, r'$\omega_r$', fontsize=10) 
    #ax3.text(1600,2.217, r'(c)', fontsize=14) 

    #ax4 = pl.subplot(gs[2, 1]) # row 0, col 0
    #ax4.plot(XX[:cutplots], avvar[:cutplots],'ko',label='trajectory average')
    #ax4.errorbar(XX[:cutplots], avvar[:cutplots], yerr=tr_avvar_std[:cutplots], fmt='o',color='black')
    #plt.axhline(y=avvar[7], color='black', linestyle=':')
    #ax4.set(xlabel=r'$\omega_c$ (cm$^{-1}$)', ylabel=r' $\langle\langle R_{SiC}\rangle^2\rangle_{tr}-\langle\langle R_{SiC}\rangle\rangle^2_{tr}$')
    #plt.locator_params(axis='x', nbins=10)
    #plt.locator_params(axis='y', nbins=5)
    #plt.axvline(x=856, color='orange', linestyle='--')
    #ax4.text(1400,avvar[7]+0.005, r'$g_0=0$', fontsize=10) 
    #ax4.text(880,0.0765, r'$\omega_r$', fontsize=10) 
    #ax4.text(1600,0.2, r'(d)', fontsize=14) 

    #ax5 = pl.subplot(gs[3, 0]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    #ax5.plot(XX[cutplots:], avdis[cutplots:],'ko',markersize=2)
    #ax5.errorbar(XX[cutplots:],avdis[cutplots:], yerr=tr_avdis_std[cutplots:], fmt='o',color='black')
    #ax5.set(xlabel=r'$\omega_c$ (cm$^{-1}$)', ylabel=r'$ \langle\langle R_{SiC}\rangle\rangle_{tr}$ ($\mathring{A}$)')
    #plt.locator_params(axis='x', nbins=10)
    #plt.locator_params(axis='y', nbins=4)
    #plt.axvline(x=856, color=cmap(3*1/7), linestyle='--')
    #plt.axvline(x=85.6, color='orange', linestyle='--')
    #plt.axhline(y=avdis[7], color='black', linestyle=':')
    #ax3.text(1400,avdis[7]+0.005, r'$g_0=0$', fontsize=10) 
    #ax5.text(88.0,2.1, r'$0.1\omega_r$', fontsize=10) 
    #ax5.text(160.0,2.217, r'(e)', fontsize=14) 

    #ax6 = pl.subplot(gs[3, 1]) # row 0, col 0
    #ax6.plot(XX[cutplots:], avvar[cutplots:],'ko',label='trajectory average')
    #ax6.errorbar(XX[cutplots:],avvar[cutplots:], yerr=tr_avvar_std[cutplots:], fmt='o',color='black')
    #plt.axhline(y=avvar[7], color='black', linestyle=':')
    #ax6.set(xlabel=r'$\omega_c$ (cm$^{-1}$)', ylabel=r' $\langle\langle R_{SiC}\rangle^2\rangle_{tr}-\langle\langle R_{SiC}\rangle\rangle^2_{tr}$')
    #plt.locator_params(axis='x', nbins=10)
    #plt.locator_params(axis='y', nbins=5)
    #plt.axvline(x=85.6, color='orange', linestyle='--')
    #ax4.text(1400,avvar[7]+0.005, r'$g_0=0$', fontsize=10) 
    #ax6.text(88.0,0.0765, r'$0.1\omega_r$', fontsize=10) 
    #ax6.text(160.0,0.2, r'(f)', fontsize=14) 

    ax7 = pl.subplot(gs[1, :]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    ax7.plot(XX, avdis,'ko',markersize=2)
    ax7.errorbar(XX,avdis, yerr=tr_avdis_std, fmt='o',color='black')
    ax7.set(xlabel=r'$\omega_c$ (cm$^{-1}$)', ylabel=r'$ \langle\langle R_{SiC}\rangle\rangle_{tr}$ ($\mathring{A}$)')
    plt.locator_params(axis='x', nbins=10)
    plt.locator_params(axis='y', nbins=4)
    plt.xlim([42.8-20,max(XX)+20])
    #plt.axvline(x=856, color=cmap(3*1/7), linestyle='--')
    plt.axvline(x=85.6, color='red', linestyle='-.')
    plt.axvline(x=856, color='orange', linestyle='--')
    plt.axhline(y=avdis[27], color='black', linestyle=':')
    print(avdis[27])
    ax7.text(1300,avdis[27]-0.025, r'$g_0=0$', fontsize=10) 
    ax7.text(96.0,2.3, r'$0.1~\omega_r$', fontsize=10) 
    ax7.text(880,2.3, r'$\omega_r$', fontsize=10) 
    #ax7.text(578,avdis[28]-0.01, r'$g_0/\hbar\omega_c$'+'\n'+r'$=0.566$', fontsize=8) 
    #plt.axvline(x=167, color='gray', linestyle='--')
    plt.axvline(x=292, color='gray', linestyle='--')
    plt.axvline(x=515, color='gray', linestyle='--')
    plt.axvline(x=771, color='gray', linestyle='--')
    plt.axvline(x=849, color='gray', linestyle='--')
    plt.axvline(x=1201, color='gray', linestyle='--')
    #plt.axvline(x=2093, color='gray', linestyle='--')
    ax7.text(1220,2.05, 'vib. eigenmodes\nwith Si-C character', fontsize=10, color='gray') 
    ax7.text(1600,2.217, r'(b)', fontsize=14) 

    #ax8 = pl.subplot(gs[3, :]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    #ax8.plot(XX, avdis_short,'ko',markersize=2)
    #ax8.errorbar(XX,avdis_short, yerr=tr_avdis_std_short, fmt='o',color='black')
    #ax8.set(xlabel=r'$\omega_c$ (cm$^{-1}$)', ylabel=r'$ \langle\langle R_{SiC}\rangle\vert_{500~fs}^{700~fs}\rangle_{tr}$ ($\mathring{A}$)')
    #plt.locator_params(axis='x', nbins=10)
    #plt.locator_params(axis='y', nbins=4)
    #plt.xlim([min(XX)-20,max(XX)+20])
    #plt.axvline(x=856, color=cmap(3*1/7), linestyle='--')
    #plt.axvline(x=85.6, color='orange', linestyle='--')
    #plt.axvline(x=856, color='orange', linestyle='--')
    #plt.axhline(y=avdis[7], color='black', linestyle=':')
    #ax3.text(1400,avdis[7]+0.005, r'$g_0=0$', fontsize=10) 
    #ax8.text(96.0,2.3, r'$0.1~\omega_r$', fontsize=10) 
    #ax8.text(880,2.3, r'$\omega_r$', fontsize=10) 
    #plt.axvline(x=167, color='gray', linestyle='--')
    #plt.axvline(x=292, color='gray', linestyle='--')
    #plt.axvline(x=515, color='gray', linestyle='--')
    #plt.axvline(x=771, color='gray', linestyle='--')
    #plt.axvline(x=849, color='gray', linestyle='--')
    #plt.axvline(x=1201, color='gray', linestyle='--')
    #plt.axvline(x=2093, color='gray', linestyle='--')
    #ax8.text(1600,2.217, r'(d)', fontsize=14) 

    ax9 = pl.subplot(gs[2, :]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    ax9.plot(spectrumx[0,:], spectrumx[1,:]/2, 'b-',label=r'$ S^{PTAF^{-}}_{\epsilon_c} / 2 $', alpha=0.5)
    ax9.plot(spectrumx[0,:], spectrumx[2,:], 'r--',label=r'$ S^{PTAF^{-}}_{\epsilon_c} \cdot \vert\langle M\vert Si-C \rangle\vert$')
    ax9.set(xlabel=r'$\omega$ (cm$^{-1}$)', ylabel=r'$ S^{PTAF^{-}}_{\epsilon_c},~S^{PTAF^{-}}_{\epsilon_c} \cdot \vert\langle M\vert Si-C \rangle\vert$ (a.u.)')
    plt.xlim([42.8-20,max(XX)+20])
    plt.ylim([0,0.0015])
    ax9.legend(frameon=False, loc='upper right')
    #plt.axvline(x=85.6, color='orange', linestyle='--')
    #plt.axvline(x=856, color='orange', linestyle='--')
    #plt.axvline(x=167, color='gray', linestyle='--')
    plt.axvline(x=292, color='gray', linestyle='--')
    plt.axvline(x=515, color='gray', linestyle='--')
    plt.axvline(x=771, color='gray', linestyle='--')
    plt.axvline(x=849, color='gray', linestyle='--')
    plt.axvline(x=1201, color='gray', linestyle='--')
    ax9.text(1600,0.0005, r'(c)', fontsize=14) 

plt.show()
    ##############


    #return

##############################

    
    

main()
plt.savefig(outfile,dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)


