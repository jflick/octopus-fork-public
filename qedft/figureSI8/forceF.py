#!/usr/bin/python                                                                                                                                             
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
from matplotlib.lines import Line2D

np.set_printoptions(threshold=1e20)
font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 16,
        }
mpl.rcParams['xtick.labelsize'] = 16
mpl.rcParams['ytick.labelsize'] = 16
mpl.rc('font', family='serif')
mpl.rc('font', serif='Times')
mpl.rcParams.update({'font.size': 16})

figure = plt.figure(figsize=(12,6))
## load trajectory
geo_data = np.loadtxt('../../F_opposite/300K/05/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data = np.loadtxt('../../F_opposite/300K/05/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_lamh = np.loadtxt('05_p10/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_lamh = np.loadtxt('05_p10/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_lam1 = np.loadtxt('05_p10_coupling2/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_lam1 = np.loadtxt('05_p10_coupling2/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_lam2 = np.loadtxt('05_p10_coupling3/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_lam2 = np.loadtxt('05_p10_coupling3/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_lam4 = np.loadtxt('05_p10_coupling4/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_lam4 = np.loadtxt('05_p10_coupling4/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_lam8 = np.loadtxt('05_p10_coupling5/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_lam8 = np.loadtxt('05_p10_coupling5/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_lam16 = np.loadtxt('05_p10_coupling10/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_lam16 = np.loadtxt('05_p10_coupling10/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position

mul=50
SiF_dist = np.zeros((int(95000/mul),7))
forces = np.zeros((int(95000/mul),7))

counter=-1
for i in range(int(95000/mul)):
   counter+=1
   SiF_dist[counter,0] = np.sqrt(np.sum(pow(geo_data[i*mul,2:4] - geo_data[i*mul,5:7],2)))*0.529177249
   SiF_dist[counter,1] = np.sqrt(np.sum(pow(geo_data_lamh[i*mul,2:4] - geo_data_lamh[i*mul,5:7],2)))*0.529177249
   SiF_dist[counter,2] = np.sqrt(np.sum(pow(geo_data_lam1[i*mul,2:4] - geo_data_lam1[i*mul,5:7],2)))*0.529177249
   SiF_dist[counter,3] = np.sqrt(np.sum(pow(geo_data_lam2[i*mul,2:4] - geo_data_lam2[i*mul,5:7],2)))*0.529177249
   SiF_dist[counter,4] = np.sqrt(np.sum(pow(geo_data_lam4[i*mul,2:4] - geo_data_lam4[i*mul,5:7],2)))*0.529177249
   SiF_dist[counter,5] = np.sqrt(np.sum(pow(geo_data_lam8[i*mul,2:4] - geo_data_lam8[i*mul,5:7],2)))*0.529177249
   SiF_dist[counter,6] = np.sqrt(np.sum(pow(geo_data_lam16[i*mul,2:4] - geo_data_lam16[i*mul,5:7],2)))*0.529177249
   #SiC_dist = np.sqrt(np.sum(pow(geo_data[i*10,5:7] - geo_data[i*10,8:10],2)))*0.529177249
   #F_force = forces_data[i*10,2:4]
   forces[counter,0] = forces_data[i*mul,2]
   forces[counter,1] = forces_data_lamh[i*mul,2]
   forces[counter,2] = forces_data_lam1[i*mul,2]
   forces[counter,3] = forces_data_lam2[i*mul,2]
   forces[counter,4] = forces_data_lam4[i*mul,2]
   forces[counter,5] = forces_data_lam8[i*mul,2]
   forces[counter,6] = forces_data_lam16[i*mul,2]

cmap = mpl.cm.get_cmap('Blues')

plt.plot(SiF_dist[:,0], forces[:,0], linewidth=3, color=cmap(0.1),linestyle='--',label=r'$g_0/\hbar\omega_c=0$')
plt.plot(SiF_dist[:,1], forces[:,1], linewidth=3, color=cmap(0.2),label=r'$g_0/\hbar\omega_c=0.238$')
plt.plot(SiF_dist[:,2], forces[:,2], linewidth=3, color=cmap(0.3),label=r'$g_0/\hbar\omega_c=0.566$')
plt.plot(SiF_dist[:,3], forces[:,3], linewidth=3, color=cmap(0.5),label=r'$g_0/\hbar\omega_c=0.849$')
plt.plot(SiF_dist[:,4], forces[:,4], linewidth=3, color=cmap(0.7),label=r'$g_0/\hbar\omega_c=1.132$')
plt.plot(SiF_dist[:,5], forces[:,5], linewidth=3, color=cmap(0.85),label=r'$g_0/\hbar\omega_c=1.415$')
plt.plot(SiF_dist[:,6], forces[:,6], linewidth=3, color=cmap(1.0),label=r'$g_0/\hbar\omega_c=2.831$')
plt.xlabel('Si-F distance ($\AA$)')#, fontdict=font)
plt.ylabel(r'$f_{F}\cdot \epsilon_x$ (a.u.)')#, fontdict=font)
plt.xlim(3.9,2.5)
plt.ylim(-0.015,0.01)
plt.legend()
figure.savefig('forceF.png',dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)
plt.close()
