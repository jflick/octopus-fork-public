#!/usr/bin/python                                                                                                                                             \

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata

np.set_printoptions(threshold=np.nan)

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 30,
        }

mpl.rcParams['xtick.labelsize'] = 30
mpl.rcParams['ytick.labelsize'] = 30

mpl.rc('font', family='serif')
mpl.rc('font', serif='Times')
#mpl.rc('text', usetex='true')
mpl.rcParams.update({'font.size': 30})

phot=0 # add photon q to the plot

vib_modes_data = np.loadtxt('vib_modes/normal_modes_fd')[:,1:]
n_vib_modes = len(vib_modes_data)+1*phot
SiCdisplacement = np.zeros(27*3)
#Si -5.820986 -1.182859 0.123530
#C -1.930749 -1.284194 0.108589
diff = [5.820986-1.930749, 1.182859-1.284194, -0.123530+0.108589]
SiCdisplacement[3] = -diff[0]
SiCdisplacement[4] = -diff[1]
SiCdisplacement[5] = -diff[2]
SiCdisplacement[6] = diff[0]
SiCdisplacement[7] = diff[1]
SiCdisplacement[8] = diff[2]
SiCdisplacement = SiCdisplacement/np.linalg.norm(SiCdisplacement)

#SiCdisplacement[3] = -1./np.sqrt(2)
#SiCdisplacement[6] = +1./np.sqrt(2)
result_data = np.zeros(27*3)

for i in range(n_vib_modes):
  norm_vib_mode = vib_modes_data[i]/np.linalg.norm(vib_modes_data[i])
  result_data[i] = np.abs(np.dot(SiCdisplacement, norm_vib_mode))
np.savetxt('norm_modes_siccontribution.txt', result_data)
