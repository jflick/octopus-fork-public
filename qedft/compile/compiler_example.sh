#! /bin/bash -l
# compilation script for octopus on MPCDF systems
# needs to be executed in a subfolder (e.g. _build) of the root directory
#
# if you want to skip the configure step, call as ./build_octopus.sh noconfig
# if you want to skip the configure step and the dependency checking,
#  call as ./build_octopus.sh noconfig nodep
if [[ ! -f ../configure.ac ]]; then
 echo “Error! Please execute this script in a subfolder of the root directory.”
 exit 1
fi
# use intel or gnu compiler
compiler=intel
#compiler=gnu
# use cuda?
#cuda=yes
# installation directory
export INSTALLDIR=$PWD/compiled
echo Using $compiler compiler.
[[ $cuda == yes ]] && echo Building with CUDA support.
echo Installing to $INSTALLDIR
module purge
if [[ $compiler == intel ]]; then
 # newest intel version
 #module load intel/19.1.1 impi mkl/2020.1
 # default intel version
 module load intel/19.1.3 impi/2019.9 libxc fftw-mpi/3.3.8 mkl/2020.1
 export CC=mpiicc
 export FC=mpiifort
 export CXX=mpiicpc
 export CFLAGS=“-O3 -xCORE-AVX512 -qopt-zmm-usage=high -fma -ip”
 export FCFLAGS=“$CFLAGS”
 export CXXFLAGS=“$CFLAGS”
 export MKL=“-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -liomp5 -lpthread -lm -ldl”
elif [[ $compiler == gnu ]]; then
 module load gcc/9 impi mkl
 export CC=mpicc
 export FC=mpif90
 export CXX=mpicxx
 export CFLAGS=“-O3 -march=skylake-avx512 -g”
 export FCFLAGS=“$CFLAGS”
 export CXXFLAGS=“$CFLAGS”
 export MKL=“-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_gf_lp64 -lmkl_gnu_thread -lmkl_core -lmkl_blacs_intelmpi_lp64 -lgomp -lpthread -lm -ldl”
else
 echo “Compiler $compiler unknown.”
 exit 1
fi
module load gsl hdf5-serial netcdf-serial libxc metis parmetis gcc/9
# RPATH needed to find libraries at runtime
export LDFLAGS=“-Xlinker -rpath=$MKL_HOME/lib/intel64:$GSL_HOME/lib:$NETCDF_HOME/lib:$ELPA_HOME/lib:$METIS_HOME/lib:$PARMETIS_HOME/lib:$FFTW_HOME/lib”
if [[ $cuda == yes ]]; then
 module load cuda/10.2
 CUDA_FLAGS=“--enable-cuda --enable-nvtx --with-cuda-prefix=$CUDA_HOME”
 LDFLAGS=“$LDFLAGS:$CUDA_HOME/lib64”
else
 CUDA_FLAGS=“”
fi
module list
if [[ “$1” != “noconfig” ]]; then
 echo “\n\nRunning configure...\n”
 pushd .. && autoreconf -i && popd
 ../configure $CUDA_FLAGS \
  FCFLAGS_FFTW=“-I$MKLROOT/include/fftw” \
  FCCPP=“cpp -ffreestanding” \
  --prefix=$INSTALLDIR \
  --enable-mpi --enable-openmp \
  --disable-gdlib \
  --with-gsl-prefix=“$GSL_HOME” \
  --with-libxc-prefix=“$LIBXC_HOME” \
  --with-blas=“$MKL” \
  --with-lapack=“$MKL” \
  --with-blacs=“$MKL” \
  --with-fftw-prefix=$FFTW_HOME \
  --with-scalapack=“$MKL” \
  --with-netcdf-prefix=“$NETCDF_HOME” \
  --with-metis-prefix=“$METIS_HOME” \
  --with-parmetis-prefix=“$PARMETIS_HOME” \
  || exit 1
fi
echo “\n\nBuilding octopus...\n”
if [[ “$2" == “nodep” ]]; then
 make NODEP=1 -j20 && make NODEP=1 install || exit 1
 echo ‘opt 1’
else
 make -j20 && make install || exit 1
 echo ‘opt 2’
fi
echo Done installing to $INSTALLDIR
mkdir -p $INSTALLDIR/.build.doc/
cp -f config.log $INSTALLDIR/.build.doc/
cp -f $0 $INSTALLDIR/.build.doc/
echo “... done”

