# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from matplotlib.path import Path
import matplotlib.patches as patches
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
from pylab import title as pytitle
import matplotlib.gridspec as gridspec

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
cmap = matplotlib.cm.get_cmap('coolwarm_r')
     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   

abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale

cmtoau = 1.0/219474.63068

def main():

        
    ############## DATA #############


    spec0 = np.loadtxt('vib_modes/infrared',skiprows=1) # PTAF
    sic_contribution = np.loadtxt('modes_sic_contribution/vib_ptaf/norm_modes_siccontribution.txt') # SiC contribution PTAF
    xcoords = [428.000000000047,570.666666666729,684.800000000074,856.000000000093,1070.00000000012,1283.99999935247,1712.00000000019]

##############################
    sigma = 3
    dx = 1
    g0 = np.arange(spec0[-1,0], spec0[0,0], dx) 

    gaussian0 = np.zeros(len(g0))

    gaussian0x = np.zeros(len(g0))

    gaussian0x_sic_weight = np.zeros(len(g0))

    for ii in range(len(spec0[:,0])):
      gaussian0 = gaussian0 + 2/3*spec0[ii,0]*cmtoau*(spec0[ii,1]**2+spec0[ii,2]**2+spec0[ii,3]**2)*np.exp(-np.power((g0-spec0[ii,0])/sigma,2)/2)/(np.sqrt(2.*np.pi)*sigma)
      gaussian0x = gaussian0x + 2*spec0[ii,0]*cmtoau*spec0[ii,1]**2*np.exp(-np.power((g0-spec0[ii,0])/sigma,2)/2)/(np.sqrt(2.*np.pi)*sigma)
      gaussian0x_sic_weight = gaussian0x_sic_weight + sic_contribution[ii]*2*spec0[ii,0]*cmtoau*spec0[ii,1]**2*np.exp(-np.power((g0-spec0[ii,0])/sigma,2)/2)/(np.sqrt(2.*np.pi)*sigma)

    fig, ax = plt.subplots(figsize=(6,3))
    plt.plot(g0,(gaussian0x),'b-',alpha=1,label=r'PTAF$^{-}$', zorder=0.9)
    plt.axvline(x=xcoords[3],ymin=0.1, color='orange', linestyle='--',zorder=0)
    #ax.grid(True)
    ax.set(xlabel=r"$\omega$ ($cm^{-1}$)", ylabel=r'$S_{\epsilon_c}(\omega)$ (a.u.)')
    ax.set_xlim(400,1300)
    ax.set_ylim(-0.0002,0.007)
    leg = plt.legend(loc='upper left',frameon=False)
    ax.text(xcoords[3]-41,0.0065, r'$\omega_r$', fontsize=12)    
    # Get the bounding box of the original legend
    bb = leg.get_bbox_to_anchor().inverse_transformed(ax.transAxes)
    # Change to location of the legend. 
    yOffset = 0.03
    bb.y0 += yOffset
    bb.y1 += yOffset
    leg.set_bbox_to_anchor(bb, transform = ax.transAxes) 
    fig.savefig('spectrum_ptaf_new-corect-order',dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)


    ZZ = [g0, gaussian0x, gaussian0x_sic_weight]
    np.savetxt('spectrumx.dat', ZZ)

    #return

##############################

    
    
plt.show()
main()


