# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from scipy.interpolate import make_interp_spline
from matplotlib.path import Path
import matplotlib.patches as patches
import matplotlib
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D

from pylab import title as pytitle
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec
from scipy.interpolate import interp1d

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}
matplotlib.rcParams['xtick.labelsize'] = 8
matplotlib.rcParams['ytick.labelsize'] = 8

matplotlib.rc('font', **font)

abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale
outfile='td_distance_lossscan_00026.pdf'

def main():

    lenmax=888
    t05_0 = np.loadtxt('../../300K_cavity_0.002600148659089/05/frames/bond_length.dat')
    t05_1 = np.loadtxt('../../300K_cavity_0.002600148659089/05_loss_1exp/bond_length.dat')
    t05_2 = np.loadtxt('../../300K_cavity_0.002600148659089/05_loss_2exp/bond_length.dat')
    t05_4 = np.loadtxt('../../300K_cavity_0.002600148659089/05_loss_4exp/bond_length.dat')
    t05_8 = np.loadtxt('../../300K_cavity_0.002600148659089/05_loss_8exp/bond_length.dat')

    t22_0 = np.loadtxt('../johannes/bond_length-12-2.dat')
    t28_0 = np.loadtxt('../johannes/bond_length-18-2.dat')
    t29_0 = np.loadtxt('../johannes/bond_length-19-2.dat')
    t22_1 = np.loadtxt('johannes/bond_length-12-31.dat')
    t28_1 = np.loadtxt('johannes/bond_length-18-31.dat')
    t29_1 = np.loadtxt('johannes/bond_length-19-31.dat')
    t22_2 = np.loadtxt('johannes/bond_length-12-32.dat')
    t28_2 = np.loadtxt('johannes/bond_length-18-32.dat')
    t29_2 = np.loadtxt('johannes/bond_length-19-32.dat')
    t22_4 = np.loadtxt('johannes/bond_length-12-33.dat')
    t28_4 = np.loadtxt('johannes/bond_length-18-33.dat')
    t29_4 = np.loadtxt('johannes/bond_length-19-33.dat')
    t22_8 = np.loadtxt('johannes/bond_length-12-34.dat')
    t28_8 = np.loadtxt('johannes/bond_length-18-34.dat')
    t29_8 = np.loadtxt('johannes/bond_length-19-34.dat')


    t12_0 = np.loadtxt('../enrico/02/0.66_omega/bond_length.dat')
    t14_0 = np.loadtxt('../enrico/04/0.66_omega/bond_length.dat')
    t18_0 = np.loadtxt('../enrico/08/0.66_omega/bond_length.dat')
    t20_0 = np.loadtxt('../enrico/10/0.66_omega/bond_length.dat')
    t12_1 = np.loadtxt('enrico/02/bond_length.dat')
    t14_1 = np.loadtxt('enrico/04/bond_length.dat')
    t18_1 = np.loadtxt('enrico/08/bond_length.dat')
    t20_1 = np.loadtxt('enrico/10/bond_length.dat')
    t12_2 = np.loadtxt('enrico/02/bond_length_2_exp.dat')
    t14_2 = np.loadtxt('enrico/04/bond_length_2_exp.dat')
    t18_2 = np.loadtxt('enrico/08/bond_length_2_exp.dat')
    t20_2 = np.loadtxt('enrico/10/bond_length_2_exp.dat')
    t12_4 = np.loadtxt('enrico/02/bond_length_4_exp.dat')
    t14_4 = np.loadtxt('enrico/04/bond_length_4_exp.dat')
    t18_4 = np.loadtxt('enrico/08/bond_length_4_exp.dat')
    t20_4 = np.loadtxt('enrico/10/bond_length_4_exp.dat')
    t12_8 = np.loadtxt('enrico/02/bond_length_8_exp.dat')
    t14_8 = np.loadtxt('enrico/04/bond_length_8_exp.dat')
    t18_8 = np.loadtxt('enrico/08/bond_length_8_exp.dat')
    t20_8 = np.loadtxt('enrico/10/bond_length_8_exp.dat')

    time = t05_1[:lenmax,0]
    c0 = abohr*(t05_0[:lenmax,1]+t12_0[:lenmax,1]+t14_0[:lenmax,1]+t18_0[:lenmax,1]+t20_0[:lenmax,1]+t22_0[:lenmax,1]+t28_0[:lenmax,1]+t29_0[:lenmax,1])/8.
    c1 = abohr*(t05_1[:lenmax,1]+t12_1[:lenmax,1]+t14_1[:lenmax,1]+t18_1[:lenmax,1]+t20_1[:lenmax,1]+t22_1[:lenmax,1]+t28_1[:lenmax,1]+t29_1[:lenmax,1])/8.
    c2 = abohr*(t05_2[:lenmax,1]+t12_2[:lenmax,1]+t14_2[:lenmax,1]+t18_2[:lenmax,1]+t20_2[:lenmax,1]+t22_2[:lenmax,1]+t28_2[:lenmax,1]+t29_2[:lenmax,1])/8.
    c4 = abohr*(t05_4[:lenmax,1]+t12_4[:lenmax,1]+t14_4[:lenmax,1]+t18_4[:lenmax,1]+t20_4[:lenmax,1]+t22_4[:lenmax,1]+t28_4[:lenmax,1]+t29_4[:lenmax,1])/8.
    c8 = abohr*(t05_8[:lenmax,1]+t12_8[:lenmax,1]+t14_8[:lenmax,1]+t18_8[:lenmax,1]+t20_8[:lenmax,1]+t22_8[:lenmax,1]+t28_8[:lenmax,1]+t29_8[:lenmax,1])/8.

##############################
    XX = np.array([0, 1, 2, 4, 8])

    gs = gridspec.GridSpec(2,2)
    gs.update(left=0.05, right=0.95, wspace=0.3, hspace=0.25)
    pl.figure(figsize=(6*2/3, 10*2/3))

    #cmap = matplotlib.cm.get_cmap('coolwarm_r')
    cmap = matplotlib.cm.get_cmap('Blues')
    ax0 = pl.subplot(gs[0, :]) # row 0, col 0
    ax0.plot(time, c0,color=cmap(1/5),markersize=1,label=r''+'{:1d}'.format(XX[0])+' $\gamma_{exp}$' )
    ax0.plot(time, c1,color=cmap(2/5),markersize=1,label=r''+'{:1d}'.format(XX[1])+' $\gamma_{exp}$' )
    ax0.plot(time, c2,color=cmap(3/5),markersize=1,label=r''+'{:1d}'.format(XX[2])+' $\gamma_{exp}$' )
    ax0.plot(time, c4,color=cmap(4/5),markersize=1,label=r''+'{:1d}'.format(XX[3])+' $\gamma_{exp}$' )
    ax0.plot(time, c8,color=cmap(5/5),markersize=1,label=r''+'{:1d}'.format(XX[4])+' $\gamma_{exp}$' )
    ax0.set_xlim(0,700)
    ax0.legend(frameon=False)
    ax0.set(xlabel=r'Time (fs)', ylabel=r'$\langle R_{SiC}(t) \rangle_{tr}$ ($\mathring{A}$)')
    ax0.text(620,2.35, r'(a)', fontsize=14) 
    plt.arrow(500, 1.85, 0, +0.05, width=0.1, head_width = 5, head_length = 0.015, color='black')

    couplingvals = 5
    files = 8
    avdis = [np.sum(c0)/lenmax,np.sum(c1)/lenmax,np.sum(c2)/lenmax,np.sum(c4)/lenmax,np.sum(c8)/lenmax]
    avdis_g0 = 2.366587507044463

    #for jj in range(couplingvals):
    #  tr_avdis_std[jj] = np.sqrt( np.sum(np.square(tr_avdis[jj,:]))/float(files) - (np.sum(tr_avdis[jj,:])/float(files))**2 )/np.sqrt(float(files))

    f2 = interp1d(XX, avdis, kind='cubic')
    xnew = np.linspace(XX[0], XX[4], num=30, endpoint=True)

    ax7 = pl.subplot(gs[1, :]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    ax7.plot(XX, avdis,'ko',markersize=2)
    ax7.plot(xnew, f2(xnew),'r:', linewidth='0.5')
    #ax7.errorbar(XX,avdis, yerr=tr_avdis_std, fmt='o',color='black')
    ax7.set(xlabel=r'Loss in units of $\gamma_{exp}$', ylabel=r'$ \langle\langle R_{SiC}\rangle\rangle_{tr}$ ($\mathring{A}$)')
    plt.locator_params(axis='x', nbins=10)
    plt.locator_params(axis='y', nbins=4)
    plt.axhline(y=avdis_g0, color='black', linestyle=':')
    ax7.text(XX[2],avdis_g0-0.025, r'$g_0=0$', fontsize=10) 
    #plt.xlim([42.8-20,max(XX)+20])
    #plt.axvline(x=856, color=cmap(3*1/7), linestyle='--')
    #plt.axvline(x=85.6, color='red', linestyle='-.')
    #plt.axvline(x=856, color='orange', linestyle='--')
    #plt.axhline(y=avdis[27], color='black', linestyle=':')
    #ax7.text(1300,avdis[27]-0.025, r'$g_0=0$', fontsize=10) 
    #ax7.text(96.0,2.3, r'$0.1~\omega_r$', fontsize=10) 
    #ax7.text(880,2.3, r'$\omega_r$', fontsize=10) 
    ax7.text(7.35,2.32, r'(b)', fontsize=14) 

plt.show()
    ##############


    #return

##############################

    
    

main()
plt.savefig(outfile,dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)


