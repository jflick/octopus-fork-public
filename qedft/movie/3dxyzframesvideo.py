# load anaconda/3_5.3.0 on eos for python 3.7 (necessary for f-string option

import matplotlib
matplotlib.use('Agg')
import os
from mpl_toolkits import mplot3d

import numpy as np
import matplotlib.pyplot as plt

#import h5py

def get_atomspecies(filename):
  data = np.genfromtxt(filename, skip_header=2, dtype=('U25',float,float,float))
  atomlist = np.zeros((27,1),dtype=str)
  for ii in range(0, len(data)):
    #print (ii, data[ii])
    atomlist[ii]=data[ii][0]
  return atomlist

cm1 = 219474.63068

natoms = 27
nsteps = 110000
temp = 300
photons = True
omega = 0.003900222988634
lam = 0.025

dataname = 'td.general/coordinates'
data = np.loadtxt(dataname)
ang=0.529177249
atomlist = get_atomspecies('last.xyz')

header = np.array([["27","","",""],["","","",""]],dtype=str)
counter = 0
for step in range(0,nsteps, 100):
  counter += 1
  dummy = np.zeros((natoms,1),dtype=str)
  dummy2= header
  coordarray = np.reshape(data[step,2:(natoms*3+2)],(natoms,3))*ang
  coordarray = coordarray.astype(str)
  dummy=atomlist
  block1 = np.append(dummy,coordarray,axis=1)
  block2 = np.append(header,block1,axis=0)
  with open("framesvmdfile.txt", "ab") as f:
    np.savetxt(f, block2,fmt='%s')


