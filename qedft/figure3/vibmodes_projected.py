import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata

np.set_printoptions(threshold=np.nan)

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 30,
        }

mpl.rcParams['xtick.labelsize'] = 30
mpl.rcParams['ytick.labelsize'] = 30

mpl.rc('font', family='serif')
mpl.rc('font', serif='Times')
#mpl.rc('text', usetex='true')
mpl.rcParams.update({'font.size': 30})

phot=1 # add photon q to the plot

dynamics_dir = 'td.general/coordinates'
data = np.loadtxt('td.general/coordinates', skiprows=6)
dataphotonq = np.loadtxt('td.general/photons_q', skiprows=5)[:,2]
forces_data = data[:,2+27*3*2:]
time = data[:,1]*0.02418884254*10
nframes = int(len(data[:,0])*0.01)
vib_modes_data = np.loadtxt('../../vib_ptaf/vib_modes/normal_modes_fd')[:,1:]
n_vib_modes = len(vib_modes_data)+1*phot
result_data = np.zeros((nframes*(n_vib_modes), 3))

fnoyes = 0

count = 0
for frame in range(nframes):
    dyn_force_data = forces_data[frame*100,3*fnoyes:]
    norm_dyn_force = dyn_force_data/np.linalg.norm(dyn_force_data)
    for i in range(n_vib_modes):
        if (i<(n_vib_modes-1*phot)) or (phot==0):
          norm_vib_mode = vib_modes_data[i]/np.linalg.norm(vib_modes_data[i])
          result_data[count,2] = np.abs(np.dot(norm_dyn_force, norm_vib_mode))
        elif (i==(n_vib_modes-1*phot)) and phot == 1:
          result_data[count,2] = dataphotonq[frame*100]/dataphotonq[0]
        result_data[count,0] = time[frame*100]
        result_data[count,1] = i+1
        count+=1
np.savetxt('frames/norm_modes_proj.txt', result_data)

xi = np.arange(0,time[nframes*100-1],time[1]*10)
yi = np.arange(1,n_vib_modes-3*fnoyes,1)
Z = griddata(result_data[:,0], result_data[:,1], result_data[:,2], xi, yi, interp='linear')
#xi = result_data[:,0]
#yi = result_data[:,1]
#Z = result_data[:,2]
X, Y = meshgrid(xi,yi)
figure = plt.figure(figsize=(12,11))
##Contour
ax1 = figure.add_subplot(111)
#N=100
#CP = pcolormesh(X, Y, Z, cmap='YlOrRd', shading='gouraud')
CP = ax1.contourf(X, Y, Z, cmap='viridis')
cbar = figure.colorbar(CP, ax=ax1)
#cbar.set_label(r'$\vert \langle f_n \vert f(t) \rangle \vert$')
plt.xlabel('Time (fs)')#, fontdict=font)
plt.ylabel('Normal Mode')#, fontdict=font)
#plt.xlim(1,1100)
#plt.ylim(1.445633,2.833)
#plt.locator_params(axis='x', nbins=5)
#plt.locator_params(axis='y', nbins=6)
#plt.tick_params(axis='x', pad=10)
#plt.legend(numpoints=1, loc=2)
figure.savefig('frames/norm_modes_proj.png')
plt.show()
plt.close()
