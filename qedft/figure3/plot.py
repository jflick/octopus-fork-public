#!/usr/bin/python                                                                                                                                             \

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import bisplrep
from matplotlib.path import Path
import matplotlib.patches as patches
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from pylab import title as pytitle
import matplotlib.gridspec as gridspec
import matplotlib.colors as colors
import matplotlib.cbook as cbook
from matplotlib.pyplot import pcolormesh

font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 14,
        }

mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12

mpl.rc('font', family='serif')
mpl.rc('font', serif='Times')
#mpl.rc('text', usetex='true')
mpl.rcParams.update({'font.size': 14})

# load all trajectories showing the reaction at 0.5*res frequency
data_00039_05 = np.loadtxt('0.0039/05/norm_modes_proj.txt')
data_00039_12 = np.loadtxt('0.0039/12/norm_modes_proj.txt')
data_00039_14 = np.loadtxt('0.0039/14/norm_modes_proj.txt')
data_00039_18 = np.loadtxt('0.0039/18/norm_modes_proj.txt')
data_00039_20 = np.loadtxt('0.0039/20/norm_modes_proj.txt')
data_00039_22 = np.loadtxt('0.0039/22/norm_modes_proj.txt')
data_00039_28 = np.loadtxt('0.0039/28/norm_modes_proj.txt')
data_00039_29 = np.loadtxt('0.0039/29/norm_modes_proj.txt')

data_000039_05 = np.loadtxt('0.00039/05/norm_modes_proj.txt')
data_000039_12 = np.loadtxt('0.00039/12/norm_modes_proj.txt')
data_000039_14 = np.loadtxt('0.00039/14/norm_modes_proj.txt')
data_000039_18 = np.loadtxt('0.00039/18/norm_modes_proj.txt')
data_000039_20 = np.loadtxt('0.00039/20/norm_modes_proj.txt')
data_000039_22 = np.loadtxt('0.00039/22/norm_modes_proj.txt')
data_000039_28 = np.loadtxt('0.00039/28/norm_modes_proj.txt')
data_000039_29 = np.loadtxt('0.00039/29/norm_modes_proj.txt')

data_000019_05 = np.loadtxt('0.00019/05/norm_modes_proj.txt')
data_000019_12 = np.loadtxt('0.00019/12/norm_modes_proj.txt')
data_000019_14 = np.loadtxt('0.00019/14/norm_modes_proj.txt')
data_000019_18 = np.loadtxt('0.00019/18/norm_modes_proj.txt')
data_000019_20 = np.loadtxt('0.00019/20/norm_modes_proj.txt')
data_000019_22 = np.loadtxt('0.00019/22/norm_modes_proj.txt')
data_000019_28 = np.loadtxt('0.00019/28/norm_modes_proj.txt')
data_000019_29 = np.loadtxt('0.00019/29/norm_modes_proj.txt')

phot = 1 # plotting also the photons_q

tsteps = 900
length = tsteps*(81+1*phot)
data_00039 = (data_00039_05[:length,:]+data_00039_12[:length,:]+data_00039_14[:length,:]+data_00039_18[:length,:]+data_00039_20[:length,:]+data_00039_22[:length,:]+data_00039_28[:length,:]+data_00039_29[:length,:])/8.0
data_000039 = (data_000039_05[:length,:]+data_000039_12[:length,:]+data_000039_14[:length,:]+data_000039_18[:length,:]+data_000039_20[:length,:]+data_000039_22[:length,:]+data_000039_28[:length,:]+data_000039_29[:length,:])/8.0
data_000019 = (data_000019_05[:length,:]+data_000019_12[:length,:]+data_000019_14[:length,:]+data_000019_18[:length,:]+data_000019_20[:length,:]+data_000019_22[:length,:]+data_000019_28[:length,:]+data_000019_29[:length,:])/8.0

# stuff below same as for single trajectory
Z_00039 = (np.reshape(data_00039[:tsteps*(81+1*phot),2],(-1,81+1*phot)))
Z_000019 = (np.reshape(data_000019[:tsteps*(81+1*phot),2],(-1,81+1*phot)))
Z_000039 = (np.reshape(data_000039[:tsteps*(81+1*phot),2],(-1,81+1*phot)))
time = (np.reshape(data_00039[:tsteps*(81+1*phot),0],(-1,81+1*phot)))
X, Y = np.meshgrid(time[:,0],np.arange(1,82+1*phot,1))
Z = np.transpose(np.abs(Z_00039-Z_000019))
if phot > 0:
  Z[-1,:] = Z[-1,:]/4.0
  safe = Z[-1,:]*1
  Z[40:,:] = Z[39:-1,:]
  Z[39,:]=safe

figure = plt.figure(figsize=(10,6))

##Contour
ax1 = figure.add_subplot(111)
CP = ax1.pcolormesh(X, Y, Z, cmap='YlOrRd', shading='nearest') #pcolormesh
cbar = figure.colorbar(CP, ax=ax1)
#cbar.set_label(r'Difference $f_p^{\omega=0.0039}-f_p^{\omega=0.0019}$ in normalized force-projection')
plt.xlabel('Time (fs)')#, fontdict=font)
plt.ylabel(r'Normal Mode (cm$^{-1}$)')#, fontdict=font)
plt.xlim(0,550)
plt.hlines(15,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(31,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(40,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(40+1*phot,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(46+1*phot,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(56+1*phot,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(64+1*phot,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(67+1*phot,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.hlines(82+1*phot,xmin=0,xmax=550,colors='gray',linestyles='dotted',linewidth=1.5)
plt.ylim(81+1*phot,1)
plt.yticks([15,31,40,40+1*phot,46+1*phot,56+1*phot,64+1*phot,67+1*phot],('2093','1201',r'$\omega_c$','849','771','515','292','167'))

figure.savefig('difference_norm_modes_proj.png',dpi=600,bbox_inches='tight', pad_inches=0.01)
np.savetxt('modeprojection_z_values.dat', Z)

figure = plt.figure(figsize=(10,6))
plt.plot(Z[40,:])
plt.plot(Z[41,:])
plt.show()

#plt.figure(2,figsize=(10,20))
#off=0
#for ii in [15,31,40,46,56,64,67]:
#  plt.plot(Z[ii,:]+off,label=ii)
#  off=off+0.25
#plt.legend(loc='right')
#plt.xlim(0,550)
#plt.savefig('Norm_modes_proj_0.035_mostimportant.png')
plt.close()
