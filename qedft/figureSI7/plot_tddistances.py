# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from scipy.interpolate import make_interp_spline
from matplotlib.path import Path
import matplotlib.patches as patches
import matplotlib
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D

from pylab import title as pytitle
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec
from scipy.interpolate import interp1d

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}
matplotlib.rcParams['xtick.labelsize'] = 8
matplotlib.rcParams['ytick.labelsize'] = 8

matplotlib.rc('font', **font)

abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale
outfile='td_distance_couplingscan_00026.pdf'

def main():

    lenmax=888
    t05_05 = np.loadtxt('../../300K_cavity_0.002600148659089/05_couplinghalf/frames/bond_length.dat')
    t05_075 = np.loadtxt('../../300K_cavity_0.002600148659089/05_coupling3over4/frames/bond_length.dat')
    t05_0875 = np.loadtxt('../../300K_cavity_0.002600148659089/05_coupling3p5over4/frames/bond_length.dat')
    t05_1 = np.loadtxt('../../300K_cavity_0.002600148659089/05/frames/bond_length.dat')

    t12_05 = np.loadtxt('../enrico/02/0.66_omega/0.5_lambda/bond_length.dat')
    t14_05 = np.loadtxt('../enrico/04/0.66_omega/0.5_lambda/bond_length.dat')
    t18_05 = np.loadtxt('../enrico/08/0.66_omega/0.5_lambda/bond_length.dat')
    t20_05 = np.loadtxt('../enrico/10/0.66_omega/0.5_lambda/bond_length.dat')
    t12_075 = np.loadtxt('../enrico/02/0.66_omega/0.75_lambda/bond_length.dat')
    t14_075 = np.loadtxt('../enrico/04/0.66_omega/0.75_lambda/bond_length.dat')
    t18_075 = np.loadtxt('../enrico/08/0.66_omega/0.75_lambda/bond_length.dat')
    t20_075 = np.loadtxt('../enrico/10/0.66_omega/0.75_lambda/bond_length.dat')
    t12_0875 = np.loadtxt('../enrico/02/0.66_omega/0.875_lambda/bond_length.dat')
    t14_0875 = np.loadtxt('../enrico/04/0.66_omega/0.875_lambda/bond_length.dat')
    t18_0875 = np.loadtxt('../enrico/08/0.66_omega/0.875_lambda/bond_length.dat')
    t20_0875 = np.loadtxt('../enrico/10/0.66_omega/0.875_lambda/bond_length.dat')
    t12_1 = np.loadtxt('../enrico/02/0.66_omega/bond_length.dat')
    t14_1 = np.loadtxt('../enrico/04/0.66_omega/bond_length.dat')
    t18_1 = np.loadtxt('../enrico/08/0.66_omega/bond_length.dat')
    t20_1 = np.loadtxt('../enrico/10/0.66_omega/bond_length.dat')

    t22_05 = np.loadtxt('../johannes/bond_length-12-28.dat')
    t28_05 = np.loadtxt('../johannes/bond_length-18-28.dat')
    t29_05 = np.loadtxt('../johannes/bond_length-19-28.dat')
    t22_075 = np.loadtxt('../johannes/bond_length-12-29.dat')
    t28_075 = np.loadtxt('../johannes/bond_length-18-29.dat')
    t29_075 = np.loadtxt('../johannes/bond_length-19-29.dat')
    t22_0875 = np.loadtxt('../johannes/bond_length-12-30.dat')
    t28_0875 = np.loadtxt('../johannes/bond_length-18-30.dat')
    t29_0875 = np.loadtxt('../johannes/bond_length-19-30.dat')
    t22_1 = np.loadtxt('../johannes/bond_length-12-2.dat')
    t28_1 = np.loadtxt('../johannes/bond_length-18-2.dat')
    t29_1 = np.loadtxt('../johannes/bond_length-19-2.dat')

    time = t05_05[:lenmax,0]
    c05 = abohr*(t05_05[:lenmax,1]+t12_05[:lenmax,1]+t14_05[:lenmax,1]+t18_05[:lenmax,1]+t20_05[:lenmax,1]+t22_05[:lenmax,1]+t28_05[:lenmax,1]+t29_05[:lenmax,1])/8.
    c075 = abohr*(t05_075[:lenmax,1]+t12_075[:lenmax,1]+t14_075[:lenmax,1]+t18_075[:lenmax,1]+t20_075[:lenmax,1]+t22_075[:lenmax,1]+t28_075[:lenmax,1]+t29_075[:lenmax,1])/8.
    c0875 = abohr*(t05_0875[:lenmax,1]+t12_0875[:lenmax,1]+t14_0875[:lenmax,1]+t18_0875[:lenmax,1]+t20_0875[:lenmax,1]+t22_0875[:lenmax,1]+t28_0875[:lenmax,1]+t29_0875[:lenmax,1])/8.
    c1 = abohr*(t05_1[:lenmax,1]+t12_1[:lenmax,1]+t14_1[:lenmax,1]+t18_1[:lenmax,1]+t20_1[:lenmax,1]+t22_1[:lenmax,1]+t28_1[:lenmax,1]+t29_1[:lenmax,1])/8.

##############################
    XX = np.array([0.5661223328, 0.8491834993, 0.9907140825, 1.132244666])

    gs = gridspec.GridSpec(2,2)
    gs.update(left=0.05, right=0.95, wspace=0.3, hspace=0.25)
    pl.figure(figsize=(6*2/3, 10*2/3))

    #cmap = matplotlib.cm.get_cmap('coolwarm_r')
    cmap = matplotlib.cm.get_cmap('coolwarm_r')
    ax0 = pl.subplot(gs[0, :]) # row 0, col 0
    ax0.plot(time, c05,color=cmap(1/4),markersize=1,label=r''+'{:.4f}'.format(XX[0])+' $g_0/\hbar\omega_c$' )
    ax0.plot(time, c075,color=cmap(2/4),markersize=1,label=r''+'{:.4f}'.format(XX[1])+' $g_0/\hbar\omega_c$' )
    ax0.plot(time, c0875,color=cmap(3/4),markersize=1,label=r''+'{:.4f}'.format(XX[2])+' $g_0/\hbar\omega_c$' )
    ax0.plot(time, c1,color=cmap(4/4),markersize=1,label=r''+'{:.4f}'.format(XX[3])+' $g_0/\hbar\omega_c$' )
    ax0.set_xlim(0,700)
    ax0.legend(frameon=False)
    ax0.set(xlabel=r'Time (fs)', ylabel=r'$\langle R_{SiC}(t) \rangle_{tr}$ ($\mathring{A}$)')
    ax0.text(500,3.3, r'(a)', fontsize=14) 
    plt.arrow(500, 1.85, 0, +0.05, width=0.1, head_width = 5, head_length = 0.05, color='black')

    couplingvals = 4
    files = 8
    avdis = [np.sum(c05)/lenmax,np.sum(c075)/lenmax,np.sum(c0875)/lenmax,np.sum(c1)/lenmax]
    avdis_g0 = 2.366587507044463

    #for jj in range(couplingvals):
    #  tr_avdis_std[jj] = np.sqrt( np.sum(np.square(tr_avdis[jj,:]))/float(files) - (np.sum(tr_avdis[jj,:])/float(files))**2 )/np.sqrt(float(files))

    f2 = interp1d(XX, avdis, kind='cubic')
    xnew = np.linspace(XX[0], XX[3], num=30, endpoint=True)

    ax7 = pl.subplot(gs[1, :]) # row 0, col 0
    #XXn = np.linspace(XX.min(), XX.max(), 70)
    #spline = make_interp_spline(XX, average_dis,k=5)
    #ax3.plot(XXn,spline(XXn),'k--')
    ax7.plot(XX, avdis,'ko',markersize=2)
    ax7.plot(xnew, f2(xnew),'r:', linewidth='0.5')
    #ax7.errorbar(XX,avdis, yerr=tr_avdis_std, fmt='o',color='black')
    ax7.set(xlabel=r'$g_0/\hbar\omega_c$', ylabel=r'$ \langle\langle R_{SiC}\rangle\rangle_{tr}$ ($\mathring{A}$)')
    plt.locator_params(axis='x', nbins=10)
    plt.locator_params(axis='y', nbins=4)
    plt.axhline(y=avdis_g0, color='black', linestyle=':')
    ax7.text(XX[2],avdis_g0-0.025, r'$g_0=0$', fontsize=10) 
    #plt.xlim([42.8-20,max(XX)+20])
    #plt.axvline(x=856, color=cmap(3*1/7), linestyle='--')
    #plt.axvline(x=85.6, color='red', linestyle='-.')
    #plt.axvline(x=856, color='orange', linestyle='--')
    #plt.axhline(y=avdis[27], color='black', linestyle=':')
    #ax7.text(1300,avdis[27]-0.025, r'$g_0=0$', fontsize=10) 
    #ax7.text(96.0,2.3, r'$0.1~\omega_r$', fontsize=10) 
    #ax7.text(880,2.3, r'$\omega_r$', fontsize=10) 
    ax7.text(1.1,2.3, r'(b)', fontsize=14) 

plt.show()
    ##############


    #return

##############################

    
    

main()
plt.savefig(outfile,dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)


