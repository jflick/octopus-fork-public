# load anaconda/3_5.3.0 on eos for python 3.7 (necessary for f-string option

import matplotlib
matplotlib.use('Agg')
import os
from mpl_toolkits import mplot3d

import numpy as np
import matplotlib.pyplot as plt

#import atomic_constants
import h5py

def get_atomspecies(filename):
  data = np.genfromtxt(filename, skip_header=2, dtype=('U25',float,float,float))
  atomlist = {}
  for ii in range(0, len(data)):
    print (ii, data[ii])
    atomlist.update({ii:data[ii][0]})
  return atomlist

cm1 = 219474.63068

natoms = 27
nsteps = 110000
temp = 300
photons = True
omega = 0.003900222988634
lam = 0.025

if photons:
  title_string = f'T = {temp} K, Photons = {photons}, omega = {omega}, lambda = {lam}'
else:
  title_string = f'T = {temp} K, Photons = {photons}'

dataname = 'td.general/coordinates'

try:
  print ('h5 file found')
  f = h5py.File(dataname + '.h5','r')
  data = f.get('/dataset2')
  data = np.array(data)
except:
  print ('h5 file not found')
  data = np.loadtxt(dataname)
  hf = h5py.File(dataname + '.h5', "w")
  hf.create_dataset('/dataset2', data=data)
  hf.close()

# Data for three-dimensional scattered points
deltat = data[2,1]-data[1,1]

colorlist= {
'C':'red',
'H':'k',
'Si': 'b',
'F': 'green'}

atomlist = get_atomspecies('last.xyz')

# 3 F
# 1 Si
# 2-16 C-C
# 4,5,6 CH

connectivity = {
  0: [2, 3], # Si-C
  1: [1, 2]  # Si-F
  }

counter = 0

bond_length_sic = []
bond_length_sif = []
bond_length_cc = []
angle_sic_to_sif = []
angle_sic_to_cc = []
temp_si = []
temp_c = []
temp_f = []
temp_cc = []
int_force_x = [0]
int_force_y = [0]
int_force_z = [0]
xforce_last = 0
yforce_last = 0
zforce_last = 0
comforce_x = []
comforce_y = []
comforce_z = []
time = []

for step in range(0,nsteps, 100):
  counter += 1
  #fig = plt.figure()
  #ax = plt.axes(projection='3d')

  #ax.set_xlabel('x [b]')
  #ax.set_ylabel('y [b]')
  #ax.set_zlabel('z [b]')

  #ax.set_zlim(10,-8)
  #ax.set_ylim(-10,10)
  #ax.set_xlim(-16,22)

  print(f'frame {step}')
  xdata = []
  ydata = []
  zdata = []
  cdata = []
  sdata = []

  xvel = []
  yvel = []
  zvel = []

  xforce = []
  yforce = []
  zforce = []

  time.append(data[step,1]*0.02418884254*10) # safes time in fs, the time 10 comes from the tdnuclear timestep

  for iatom in range(0,natoms):
    xdata.append(data[step,3*iatom + 2])
    ydata.append(data[step,3*iatom + 3])
    zdata.append(data[step,3*iatom + 4])
    cdata.append(colorlist[atomlist[iatom]])
    sdata.append(50)

    xvel.append(data[step,(3*natoms+2)+3*iatom])
    yvel.append(data[step,(3*natoms+2)+3*iatom+1])
    zvel.append(data[step,(3*natoms+2)+3*iatom+2])

    xforce.append(data[step,(2*3*natoms+2)+3*iatom])
    yforce.append(data[step,(2*3*natoms+2)+3*iatom+1])
    zforce.append(data[step,(2*3*natoms+2)+3*iatom+2])

  #ax.scatter3D(xdata, ydata, zdata, color=cdata, s=sdata,zorder=10)
  #ax.text(2.5,5,-4,f't={data[step,1]:04.02f} au')
  #ax.set_title(title_string)
  #plt.savefig(f'frames/frames{counter:05}.png', bbox_inches='tight')
  #plt.close(fig)

  bolzmann = 0.316681e-5
  nuclearmass = 1836.15267343
  temp_si.append(28*nuclearmass/3/bolzmann*(abs(xvel[1])**2 + abs(yvel[1])**2 + abs(zvel[1])**2))
  temp_c.append(12*nuclearmass/3/bolzmann*(abs(xvel[2])**2 + abs(yvel[2])**2 + abs(zvel[2])**2))
  temp_f.append(19*nuclearmass/3/bolzmann*(abs(xvel[0])**2 + abs(yvel[0])**2 + abs(zvel[0])**2))
  temp_cc.append(12*nuclearmass/3/bolzmann*(abs(xvel[15])**2 + abs(yvel[15])**2 + abs(zvel[15])**2))

  bond_length_sic.append(np.sqrt(abs(xdata[1]-xdata[2])**2 + abs(ydata[1]-ydata[2])**2 + abs(zdata[1]-zdata[2])**2))
  bond_length_sif.append(np.sqrt(abs(xdata[0]-xdata[1])**2 + abs(ydata[0]-ydata[1])**2 + abs(zdata[0]-zdata[1])**2))
  bond_length_cc.append(np.sqrt(abs(xdata[15]-xdata[2])**2 + abs(ydata[15]-ydata[2])**2 + abs(zdata[15]-zdata[2])**2))
  angle_sic_to_sif.append(np.arccos(((xdata[1]-xdata[0])*(xdata[1]-xdata[2]) + (ydata[1]-ydata[0])*(ydata[1]-ydata[2]) + (zdata[1]-zdata[0])*(zdata[1]-zdata[2]))/(bond_length_sic[-1]*bond_length_sif[-1])))
  angle_sic_to_cc.append(np.arccos(((xdata[2]-xdata[15])*(xdata[1]-xdata[2]) + (ydata[2]-ydata[15])*(ydata[1]-ydata[2]) + (zdata[2]-zdata[15])*(zdata[1]-zdata[2]))/(bond_length_sic[-1]*bond_length_cc[-1])))

  comforce_x.append(np.sum(xforce))
  comforce_y.append(np.sum(yforce))
  comforce_z.append(np.sum(zforce))

  if step == 0:
    forcexzero = np.sum(xforce)*deltat/2
    forceyzero = np.sum(yforce)*deltat/2
    forcezzero = np.sum(zforce)*deltat/2
  else:
    int_force_x.append( int_force_x[-1] + np.sum(xforce)*deltat/2 + xforce_last*deltat/2 )
    int_force_y.append( int_force_y[-1] + np.sum(yforce)*deltat/2 + yforce_last*deltat/2 )
    int_force_z.append( int_force_z[-1] + np.sum(zforce)*deltat/2 + zforce_last*deltat/2 )

  xforce_last = np.sum(xforce)
  yforce_last = np.sum(yforce)
  zforce_last = np.sum(zforce)

int_force_x[0] = forcexzero
int_force_y[0] = forceyzero
int_force_z[0] = forcezzero

fig = plt.figure()
ax = plt.axes()
ax.plot(time, bond_length_sic, label='Si-C(=C)')
ax.plot(time, bond_length_sif, label='Si-F')
ax.plot(time, bond_length_cc, label='(Si)-C=C-')
ax.set_xlabel('time (fs)')
ax.set_ylabel('distance in bohr')
legend=ax.legend(loc=4)
plt.savefig(f'frames/bond_length.png', bbox_inches='tight')

fig = plt.figure()
ax = plt.axes()
ax.plot(bond_length_sif, np.degrees(angle_sic_to_sif), label='Si-F to Si-C(=C) angle')
ax.plot(bond_length_sif, np.degrees(angle_sic_to_cc), label='-C=C- to Si-C(=C) angle')
ax.set_xlabel('Si-F distance in bohr')
ax.set_ylabel('angle (degrees)')
legend=ax.legend(loc=4)
plt.savefig(f'frames/bond_angle.png', bbox_inches='tight')

fig = plt.figure()
ax = plt.axes()
ax.semilogy(bond_length_sif, temp_si, label='Si')
ax.semilogy(bond_length_sif, temp_c, label='(Si-)C(=C)')
ax.semilogy(bond_length_sif, temp_f, label='F')
ax.semilogy(bond_length_sif, temp_cc, label='(Si-C=)C-')
ax.set_xlabel('Si-F distance in bohr')
ax.set_ylabel('temperature according to 3/2 kT = mv^2/2 (K)')
legend=ax.legend(loc=4)
plt.savefig(f'frames/temperature.png', bbox_inches='tight')

fig = plt.figure()
ax = plt.axes()
ax.semilogy(time, temp_si, label='Si')
ax.semilogy(time, temp_c, label='(Si-)C(=C)')
ax.semilogy(time, temp_f, label='F')
ax.semilogy(time, temp_cc, label='(Si-C=)C-')
ax.set_xlabel('time (fs)')
ax.set_ylabel('temperature according to 3/2 kT = mv^2/2 (K)')
legend=ax.legend(loc=4)
plt.savefig(f'frames/temperature_time.png', bbox_inches='tight')

fig = plt.figure()
ax = plt.axes()
ax.plot(time, int_force_x, label='Integrated COM force x')
ax.plot(time, int_force_y, label='Integrated COM force y')
ax.plot(time, int_force_z, label='Integrated COM force z')
ax.set_xlabel('time (fs)')
ax.set_ylabel('COM force in a.u.')
legend=ax.legend(loc=4)
plt.savefig(f'frames/comforce.png', bbox_inches='tight')

bond_length_data = np.zeros((len(time),2))
bond_length_data[:,0] = time
bond_length_data[:,1] = bond_length_sic
np.savetxt('frames/bond_length.dat', bond_length_data)


