# Code version
The original real-time QEDFT calculations without loss use the git-commit bec5ea6cff8e354cda92a37ed1421fa45665dd92 for octopus. An updated version can be found in this gitlab repository.
All calculations have been performed on different HPC units that require specific compilation approaches. An example code to compile the Octopus code on the HPC 'Cobra' can be found in the directory 'compile'. The Octopus code comes with a testsuite and it is advised to ensure that the necessary functionalities are operational.

# Inputs
This section includes the necessary information to reproduce the real-time QEDFT calculations.

## Initial-state geometry
The geometry is relaxed under the condition that the F-Si distance and angle is fixed. 
The corresponding geometry optimization is performed with 'input_go' in Octopus.

## Ground-state runs
All QEDFT calculations are based on the same input-file 'inp' in which CalculationMode = gs and all photonic components are commented out. 
The ground-state geometry 'last.xyz' is the same for all trajectories.

## Real-time QEDFT calculations
From here on, the real-time QEDFT calculations are performed using the 'inp' file provided.
Due to the large size of the raw data (multiple TBs), we are only providing inputs and plotting scripts if not mentioned otherwise.
We decided to upload only template files for the QEDFT runs whenever possible to keep the archive concise.
The calculations without cavity-mode are performed with photon=no

The indicated dummy-variables ($...$) have been adjusted according to the following list. 
Each parameter-combination was propagated for all 8 reactive trajectories
(see 8 files in the directory initial_velocities) with the initial velocities min-velocities.xyz.
We provide an example slurm-script to submit a job on the HPC.

### Frequency scan (figure 4 main)
N	  $Frequency [a.u.]$      $Coupling$	        g/\omega	        $initial q$	      pe  	Frequency [cm^-1]
1	  0.001950111494317	  0.0707106781186547	1.13224466567248	-318.160278801041	x     	    428
2	  0.00260014865908933	  0.0816496580927726	1.13224466567248	-275.534883916841	x	    570.666666666729
3	  0.003120178391	  0.0894427190999916	1.13224466565564	-251.527785493888	x	    684.800000020442
4	  0.003900222988634	  0.1	                1.13224466567248	-224.973290644419	x	    856.000000000093
5	  0.0048752787357925	  0.11180339887499	1.13224466567248	-201.222228401095	x	    1070.00000000012
6	  0.00585033448	          0.122474487139159	1.13224466595804	-183.689922703884	x	    1283.99999935247
7	  0.007800445977268	  0.14142135623731	1.13224466567248	-159.080139400521	x	    1712.00000000019
8	  0.0009750557471585	  0.05	                1.13224466567248	-449.946581288838	x	    214.000000000023
9	  0.00146258362073775	  0.0612372435695795	1.13224466567248	-367.379845222455	x	    321.000000000035
10	  0.0001950111494317	  0.0223606797814661	1.132244666	        -1006.11114229651	x	    42.8000000000047
11	  0.00026001486590893	  0.0258198889821849	1.132244666	        -871.317808259357	x	    57.0666666666729
12	  0.00031201783909072	  0.0282842712556436	1.132244666	        -795.400697232685	x	    68.4800000000075
13	  0.0003900222988634	  0.0316227766108312	1.132244666	        -711.428011345205	x	    85.6000000000093
14	  0.00048752787357925	  0.0353553390695545	1.132244666	        -636.320557786148	x	    107.000000000012
15	  0.0005850334482951	  0.0387298334732774	1.132244666	        -580.878538839572	x	    128.400000000014
16	  0.0007800445977268	  0.0447213595629322	1.132244666	        -503.055571148255	x	    171.200000000019
17	  0.001696597	          0.0659545298093586	1.132244666	        -341.103623094362	x	    372.359999987796
18	  0.00224262821846455	  0.0758287544624502	1.132244666	        -296.685989858871	x	    492.200000000053
19	  0.0035102006897706	  0.0948683298324936	1.132244666	        -237.142670448402	x	    770.400000000084
20	  0.00421224082772472	  0.103923048484194	1.132244666	        -216.480649919339	x	    924.480000000101
21	  0.0044852564369291	  0.107238052978656	1.132244666	        -209.788675312251	x	    984.400000000107			
22	  0.00120906912647654	  0.0556776436444059	1.132244666	        -404.063958258365	x	    265.360000000029
23	  0.00208661929891919	  0.073143694212797	1.132244666	        -307.57715097088	x	    457.96000000005
24	  0.0052653010346559	  0.116189500419832	1.132244666	        -193.626179613191	x	    1155.60000000013
25	  0.0064353679312461	  0.128452325823808	1.132244666	        -175.141469281886	x	    1412.40000000015
26	  0.0068253902301095	  0.132287565591496	1.132244666	        -170.063822528333	x	    1498.00000000016
27	  0.007215412529	  0.136014705126954	1.132244666	        -165.403652909872	x	    1583.60000000612

#### Processing frequency scan
The necessary information is obatined from the coordinates files by running in each run-directory 
the python-script '3dplotsce.py'. The resulting 'bond_length.dat' files located in the 'frames' directory are structured
and processed by the plotting-script 'plot_tddistances.py' located in the directory figure4. The spectrum in x-direction is also
provided (spectrumx). Given the separation of the runs over 3 authors, the files demanded manual structuring. The provided script is
linked to this manually structured data.

### Coupling scan (figure 7 SI)
28	0.00260014865908933	0.0408248290463863	0.56612233283624	-137.767441958421	x	0.666666666666667	570.666666666729
29	0.00260014865908933	0.0612372435695795	0.84918349925436	-206.651162937631	x	0.666666666666667	570.666666666729
30	0.00260014865908933	0.071443450831176	0.99071408246342	-241.093023427236	x	0.666666666666667	570.666666666729

#### Processing coupling scan
The data is processed similar to the previous section. The provided script is linked to this manually structured 
data. Script and data can be found in the directory figureSI7. This is the only data that we provide here explicitly. 
It can serve as possibility for direct comparison.

### Processing Bond-correlation (fig. SI 6)
The data is processed similar to the previous section. The provided script is linked to this manually structured 
data.

### Friction scan (figure 10 SI)
Similar to figure 7 SI, an exemplary input file and plotting script can be found in figureS10. The commented $friction$ value is changed in multiples {0,1,2,4,8} of 0.0000685. 
Frequency and coupling are 0.002600148659 and 0.08164965809, the initial q value is -275.5348839.
