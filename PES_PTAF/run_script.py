#!/usr/bin/python     
  
import os, sys, re
import numpy as np

#dist_range = np.arange(1.4,2.01,0.02)
dist_range = np.arange(-0.38,2.02,0.02)
F_dist_range = np.arange(-0.30,-0.31,-0.02)

ref_dist = 2.08572
ref_F_dist = 1.7345

for i in dist_range:
  for j in F_dist_range:

      if (abs(i) < 1e-8):
         i = 0.0
      elif (abs(j) < 1e-8):
         j = 0.0
      else:
         i = i.round(2)
         j = j.round(2)

      dist = ref_dist + i
      F_dist = ref_F_dist + j

      os.system('cp -r start_dir geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j))

      if (os.path.exists('./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)) == False):
         print "No DIR!!", i
         exit()

      run_out = open('./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/inp_act', 'w')
      run_inp = open('./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/inp', 'r')
      for line in run_inp:
        if line.find('XXX') > -1:
          newline = re.sub('XXX', str(dist), line)
          run_out.write(newline)
        elif line.find('YYY') > -1:
          newline = re.sub('YYY', str(F_dist), line)
          run_out.write(newline)
        else:
          run_out.write(line)
      run_inp.close()
      run_out.close()

      os.system('mv ./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/inp_act ./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/inp')

      run_out = open('./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/run_act.sh', 'w')
      run_inp = open('./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/run.sh', 'r')
      for line in run_inp:
        if line.find('XXX') > -1:
          newline = re.sub('XXX', '%.*f' % (2, i), line)
          newline = re.sub('YYY', '%.*f' % (2, j), newline)
          run_out.write(newline)
        else:
          run_out.write(line)
      run_inp.close()
      run_out.close()

      os.system('mv ./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/run_act.sh ./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j)+'/run.sh')

      os.chdir('./geo_'+'%.*f' % (2, i)+'_'+'%.*f' % (2, j))
      os.system('sbatch run.sh')
      os.chdir('../')
