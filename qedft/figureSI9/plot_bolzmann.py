# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from matplotlib.path import Path
import matplotlib.patches as patches
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from pylab import title as pytitle
import matplotlib.gridspec as gridspec
import matplotlib.colors as colors
import matplotlib.cbook as cbook

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

#from latexify import latexify
#from latexify import format_axes

     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   



abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale
outfile='bolzmann_all30.pdf'

def main():

    plt.rcParams.update({'font.size': 16})

    #golden_mean = (math.sqrt(5)-1.0)/2.0    # Aesthetic ratio
    #latexify(fig_width=6, fig_height=2.5, columns=1, PARAMS_TEXTSIZE=6)
    #fig = plt.figure() #(num=None, figsize=(8, 6), dpi=300, facecolor='w', edgecolor='k')
    #gs1 = gridspec.GridSpec(2,1)
    #gs1.update(hspace=0.7, wspace = 0.7,left = 0.1,right=0.95,bottom=0.7, top=1.1)
    hight=6
    width=hight*1.61803398875
    fig, (ax0, ax1) = plt.subplots(nrows=1,ncols=2,figsize=(width*2, hight), sharey=True)

    ##############
    files = 30 # how many frequencies you want to load, usually I calculated 20 different cavity frequencies -> should be 20
    K300 = np.zeros((3*27,files))
    for ii in range(1,files+1):
      DK300 = np.reshape(np.loadtxt('../'+"%02d"%ii+'/min-velocities.xyz',skiprows=2,usecols = (1,2,3)),(3*27,1))
      K300[:,ii-1] = DK300[:,0]

##############################
    #fig.subplots_adjust(wspace=0.05)
    plt.tight_layout()

    bins = np.linspace(0,2.8e-3,15)
    summefortraj = np.zeros((27,10))
    for ii in range(10):
      for kk in range(27):
        summefortraj[kk,ii] = np.sqrt(sum(K300[kk*3:(kk+1)*3,ii]**2))
    #print((summefortraj))
    list_of_colors = color = ['gray','gray','gray','gray','red','gray','gray','gray','gray','gray']
    ax0.hist(summefortraj,bins,label=['01','02','03','04','05','06','07','08','09','10'], color=list_of_colors, alpha=0.6) #,label=str(ii))
    ax0.legend(loc='upper right', frameon=False, prop={'size': 14})
    ax0.grid(True)
    ax0.set(xlabel=r'$\vert v_{atom} \vert$ (a.u.)') # ylabel=r' For each run j (see legend): $\sum_i\vert v(atom_i,run_j) \vert$', 
    ax0.set_xlim(0,2.8e-3)
    print(np.sum(summefortraj,axis=0))

    summefortraj = np.zeros((27,30))
    for ii in range(10,files):
      for kk in range(27):
        summefortraj[kk,ii] = np.sqrt(sum(K300[kk*3:(kk+1)*3,ii]**2))
    list_of_colors = color = ['gray','red','gray','red','gray','gray','gray','red','gray','red', 'gray','red','gray','gray','gray','gray','gray','red','red','gray']
    ax1.hist(summefortraj[:,10:],bins, color=list_of_colors, alpha=0.6, label=['11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']) #,alpha=0.4,label=str(ii))
    ax1.legend(loc='upper right', frameon=False, prop={'size': 14}, ncol=2)
    ax1.grid(True)
    ax1.set(xlabel=r'$\vert v_{atom} \vert$ (a.u.)')

    #ax1.plot(range(1,files+1),summefortraj[0,:],label='F')
    #ax1.plot(range(1,files+1),summefortraj[1,:],label='Si')
    #ax1.plot(range(1,files+1),summefortraj[2,:],label='(Si)-C=(C)')
    #ax1.plot(range(1,files+1),summefortraj[15,:],label='(Si-C)=C-(C)')
    #ax1.plot(range(1,files+1),np.sum(summefortraj[3:14,:],axis=0)/20,label=r'$\frac{1}{20}$Methyls')
    #ax1.plot(range(1,files+1),np.sum(summefortraj[16:,:],axis=0)/20,label=r'$\frac{1}{20}$Benzene')
    #ax1.plot(range(1,files+1),np.sum(summefortraj,axis=0)/50,label=r'$ \frac{1}{50} \sum_{i,j} \vert v_{a_i,r_j}\vert$')
    #ax1.legend(loc='upper right')
    #ax1.grid(True)
    #ax1.yaxis.set_major_formatter(FormatStrFormatter('%.0e'))
    #ax1.set(title='v for atoms',xlabel='run',ylabel=r'$\vert v \vert$')



    ##############
plt.show()
##############################
main()
plt.savefig(outfile,dpi=1200,bbox_inches='tight', pad_inches=0.01)
#plt.show()
