# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from matplotlib.path import Path
import matplotlib.patches as patches
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
from pylab import title as pytitle
import matplotlib.gridspec as gridspec

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
cmap = matplotlib.cm.get_cmap('coolwarm_r')
     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   

abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale

cmtoau = 1.0/219474.63068

def main():

        
    ############## DATA #############

    xcoords = [428.000000000047,570.666666666729,684.800000000074,856.000000000093,1070.00000000012,1283.99999935247,1712.00000000019]

##############################


    fig, ax = plt.subplots(figsize=(6,4))
    data = np.loadtxt("infrared-0.00",skiprows=1)
    plt.plot(data[:,0],data[:,2]+16,'k-',alpha=1,label=r'$g_0/\hbar\omega_c = 0$', zorder=1, linewidth=2)
    data = np.loadtxt("infrared-0.025",skiprows=1)
    plt.plot(data[:,0],data[:,2]+10,'r-',alpha=1,label=r'$g_0/\hbar\omega_c = 0.283$', zorder=1, linewidth=2)
    data = np.loadtxt("infrared-0.04",skiprows=1)
    plt.plot(data[:,0],data[:,2]+4.5,'g-',alpha=1,label=r'$g_0/\hbar\omega_c = 0.453$', zorder=1, linewidth=2)
    #data = np.loadtxt("infrared-0.06",skiprows=1)
    #plt.plot(data[:,0],data[:,2]+0,'b-',alpha=1,label=r'$g_0/\hbar\omega_c = 0.679$', zorder=1, linewidth=2)
    data = np.loadtxt("infrared-0.1",skiprows=1)
    plt.plot(data[:,0],data[:,2]+0,'b-',alpha=1,label=r'$g_0/\hbar\omega_c = 1.132$', zorder=1, linewidth=2)
    plt.axvline(x=xcoords[3], color='orange', linestyle='--',zorder=0, linewidth=2)
    ax.text(xcoords[3]+0.5,22, r'$\omega_r$', fontsize=16)    
    #ax.set(xlabel=r"$\omega$ ($cm^{-1}$)")
    plt.xticks(fontsize=16)
    plt.xlabel(r"$\omega$ ($cm^{-1}$)",fontsize=16)
    ax.get_yaxis().set_visible(False)
    ax.annotate('',xytext=(775, 14.), xy=(874 ,14.),
            arrowprops=dict(arrowstyle="|-|, widthA=0.3, widthB=0.3", color = 'red'))
    ax.text(650,1.15, r'Rabi-splitting', color='blue', fontsize=16)    
    ax.annotate('',xytext=(701, 8.), xy=(876 ,8.),
            arrowprops=dict(arrowstyle="|-|, widthA=0.3, widthB=0.3", color = 'green'))
    ax.annotate('',xytext=(442.5, 2.5+0.6), xy=(880 ,2.5+0.6),
            arrowprops=dict(arrowstyle="|-|, widthA=0.3, widthB=0.3", color = 'blue'))
    ax.set_xlim(400,900)
    #ax.set_ylim(-0.0002,0.007)
    plt.legend(loc='upper left',frameon=False,fontsize=16,bbox_to_anchor=(-0.074, 1.25),ncol=2)
    #ax.text(xcoords[3]+5,0.0065, r'$\omega_r$', fontsize=10)    
    #ax.text(695,1.45, r'PTA', fontsize=10)    
    #ax.text(695,0.05, r'PTA-F', fontsize=10)    
    fig.savefig('plot_absorptionPTA_inset',dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)

    ##############


    #return

##############################

    
    
plt.show()
main()


