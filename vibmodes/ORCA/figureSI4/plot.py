#!/usr/bin/python     

import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
import matplotlib.collections as mcoll
import matplotlib.colors as mcolors
from scipy.interpolate import interp1d

np.set_printoptions(threshold=sys.maxsize)

font = {'family' : 'sans-serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 25,
        }

mpl.rcParams['xtick.labelsize'] = 25
mpl.rcParams['ytick.labelsize'] = 25
#mpl.rcParams.update({'figure.autolayout': True})

mpl.rc('font', family='sans-serif')
mpl.rc('font', serif='Arial')
#mpl.rc('text', usetex='true')
mpl.rcParams.update({'font.size': 25})

data_vacuum_pbe = np.loadtxt('vacuum/PBE/spectrum_vacuum_PBE.dat',skiprows=1)
sic_vacuum_pbe = np.loadtxt('vacuum/PBE/SiC_spectrum.dat')

data_methanol_pbe = np.loadtxt('methanol/PBE/spectrum_methanol_PBE.dat',skiprows=1)
sic_methanol_pbe = np.loadtxt('methanol/PBE/SiC_spectrum.dat')

data_methanol_b3lyp = np.loadtxt('methanol/B3LYP/6-311+G2pd/spectrum_methanol_B3LYP_6311.dat',skiprows=1)
sic_methanol_b3lyp = np.loadtxt('methanol/B3LYP/6-311+G2pd/SiC_spectrum.dat')

#R_new = np.linspace(0, 3000, num=201, endpoint=True)
#f_data_vacuum_pbe = interp1d(data_vacuum_pbe[:,0], data_vacuum_pbe[:,1], kind='cubic')

figure = plt.figure(figsize=(14,14))

##Contour
ax1 = figure.add_subplot(311, zorder=10)
ax1.axes.get_xaxis().set_visible(False)

#plt.hlines(0.0, 0, 3000, color='k', linewidth=3.0, zorder=10)
markerline, stemlines, baseline = ax1.stem(sic_vacuum_pbe[:,0], sic_vacuum_pbe[:,1]*150, linefmt='r-',markerfmt=' ',basefmt=' ')
plt.setp(stemlines, 'linewidth', 3.5)
ax1.plot(data_vacuum_pbe[:,0], data_vacuum_pbe[:,1], linewidth=3.0, color='b', linestyle='-', label='PBE vacuum 631+G(p,d)', zorder=10)

#plt.ylabel('Intensity (arb. u)',labelpad=15)#, fontdict=font)
plt.xlim(0,3200)
plt.ylim(-5,110)
#ax1.xaxis.set_ticks([-2,0, 2])
#plt.locator_params(axis='x', nbins=7)
#plt.setp(ax1.spines.values(), linewidth=3)
#ax1.tick_params(length=6, width=3)
plt.locator_params(axis='y', nbins=3)
#plt.tick_params(axis='x', pad=10)
plt.legend(numpoints=1, loc=1)
#plt.legend(bbox_to_anchor=(0,1,1,2), loc="lower left", mode="expand", ncol=2)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

ax2 = figure.add_subplot(312, sharex=ax1, zorder=10)
ax2.axes.get_xaxis().set_visible(False)

#plt.hlines(0.0, -10, 10, color='k', linewidth=3.0, zorder=10)
markerline, stemlines, baseline = ax2.stem(sic_methanol_pbe[:,0], sic_methanol_pbe[:,1]*150, linefmt='r-',markerfmt=' ',basefmt=' ')
plt.setp(stemlines, 'linewidth', 3.5)
ax2.plot(data_methanol_pbe[:,0], data_methanol_pbe[:,1], linewidth=3.0, color='b', linestyle='-', label='PBE methanol 631+G(p,d)', zorder=10)

plt.ylabel('Intensity (arb. u)',labelpad=15)#, fontdict=font)
plt.xlim(0,3200)
plt.ylim(-5,110)
#ax1.xaxis.set_ticks([-2,0, 2])
#plt.locator_params(axis='x', nbins=7)
#plt.setp(ax1.spines.values(), linewidth=3)
#ax1.tick_params(length=6, width=3)
plt.locator_params(axis='y', nbins=3)
#plt.tick_params(axis='x', pad=10)
plt.legend(numpoints=1, loc=1)
#plt.legend(bbox_to_anchor=(0,1,1,2), loc="lower left", mode="expand", ncol=2)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

ax3 = figure.add_subplot(313, sharex=ax1, zorder=10)

#plt.hlines(0.0, -10, 10, color='k', linewidth=3.0, zorder=10)
markerline, stemlines, baseline = ax3.stem(sic_methanol_b3lyp[:,0], sic_methanol_b3lyp[:,1]*150, linefmt='r-',markerfmt=' ',basefmt=' ')
plt.setp(stemlines, 'linewidth', 3.5)
ax3.plot(data_methanol_b3lyp[:,0], data_methanol_b3lyp[:,1], linewidth=3.0, color='b', linestyle='-', label='B3LYP methanol 6311+G(2p,d)', zorder=10)

plt.xlabel('$\omega$ (cm$^-1$)',labelpad=15)#, fontdict=font)
#plt.ylabel('Intensity (arb. u)',labelpad=15)#, fontdict=font)
plt.xlim(0,3200)
plt.ylim(-5,110)
#ax1.xaxis.set_ticks([-2,0, 2])
#plt.locator_params(axis='x', nbins=7)
#plt.setp(ax1.spines.values(), linewidth=3)
#ax1.tick_params(length=6, width=3)
plt.locator_params(axis='y', nbins=3)
#plt.tick_params(axis='x', pad=10)
plt.legend(numpoints=1, loc=1)
#plt.legend(bbox_to_anchor=(0,1,1,2), loc="lower left", mode="expand", ncol=2)
#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

plt.subplots_adjust(wspace=0.5, hspace=0)

figure.savefig('plot_functionals.png', dpi=300)
#plt.show()
plt.close()
                   
