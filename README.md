# Repository to arxiv:2104.12429, Shining Light on the Microscopic Resonant Mechanism Responsible for Cavity-Mediated Chemical Reactivity by C. Schaefer, J. Flick, E. Ronca, P. Narang, and A. Rubio
This repository includes the necessary scripts, inputs and parameters to reproduce the shown data.
The repo is structured into subdirectories that include the necessary information to reproduce a specific set of calculations, e.g., the potential-energy surface.
Please read first carefully the main paper and SI.

The size of the output-files necessary to obtain the figure is very large and even derivative data is still substantually beyond the maximum storage of the repository.
For this reason, we decided to limit the content in this repository to the inputs and scripts necessary to obtain the data that can then be post-processed by the
corresponding scripts.

This is repository contains the following 5 branches:

"inputs_and_descriptions": Default branch containing input files

"photon-ground-mode-with-finite-q": Octopus code to obtain vibro-polaritonic spectra see:
https://pubs.acs.org/doi/full/10.1021/acs.jctc.1c01035

"minimize_total_forces_develop": Octopus code to perform constrained geometry relaxations

"photon-phonon-develop-10.1": Octopus code to perform coupled electron-photon dynamics see:
https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.121.113002

"photon-phonon-develop": Ocoptus code to perform coupled electron-photon dynamics with dissipation

To access the different code-version, use git checkout "branchname"

## Potential-energy surface (PES)
See: PES_PTAF
All PES and transition-state calculations have been obtained with ORCA 4.0
The PES is used only for visualization. It is not employed in any way in the actual QEDFT or other calculations.

### Transition-state curvatures
See: PES_PTAF/transitionstate
This subdirectory includes an example input to recover the transition-state. The same calculation can be performed for each combination of solvent and basis.
Orca prints the hessian and eigen-frequencies which are then shown in the SI.

## Initial-state for real-time calculations
See: qedft/initial_static
Before we perform any real-time calculation to investigate the reactivity, we have to find a reasonable initial-state.
For an extended discussion please refer to the paper. The initial-state has been first obtained from ORCA for a fixed
Si-F distance and then re-optimized with Octopus (same restrictions) in order to minimize the forces on the grid.
Inputs and outputs are provided. After obtaining the optimal geometry, a ground-state calculation was performed.
This configuration provides the static initial state.

## Quantum-electrodynamical density-functional theory (QEDFT) calculations
See: qedft
The real-time QEDFT calculations use the git-commit bec5ea6cff8e354cda92a37ed1421fa45665dd92 for the Octopus code.
All calculations start from the same initial-state as obtained in the previous section but in addition
we assign initial velocities. 10 of those have been sampled from a Boltzmann-distribution at 300K. A single of those
showed the reaction within a time-frame of 1ps. We then sampled additional 20 trajectories with a narrow distribution
of 20K around that single reactive trajectory. In total, this provided 8 reactive trajectories.
Those 8 trajectories have been propagated as detailed in the README.md located in qedft.
An overview over the associated Boltzmann distribution can be found in the SI and the directory figureSI9.

## Vibrational modes (DFT and QEDFT)
See: vibmodes
Those vibrational modes have been either shown directly or have been used in the post-processing of figure 3 and figure 4.
The corresponding scripts are located in their respective directories (e.g. qedft).
All inputs for figure SI 4 are in vibmodes/ORCA. All inputs for figure 1d, the post-processing of figure 3 and 4 are in vibmodes/Octopus.
All inputs for figure SI 2 are in vibmodes/OctopusQEDFT.
