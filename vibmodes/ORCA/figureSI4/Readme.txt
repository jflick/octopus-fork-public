# Instructions to generate Figure S4

1) The inp_freq_fig_s4 can be used to run ORCA 4.2.1 and obtain the output containing all the information about the IR spectrum.

2) The output of ORCA can be opened using a specific version of Avogadro as explained at the following website:
   https://www.orcasoftware.de/tutorials_orca/first_steps/GUI.html

3) Using avogadro the IR peakes can be broadened and the correpsonding absorbance spectrum can ba saved on file.

4) The plot.py file can be used to generate the plot from the spectra files. 
