# Copyright (C) 2018 C.Schaefer
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import os,sys,re,string,time, csv, math

import numpy as np
import matplotlib.pyplot as plt
from fractions import Fraction  
from scipy.interpolate import griddata
from scipy.interpolate import make_interp_spline
from matplotlib.path import Path
import matplotlib.patches as patches
import matplotlib
from pylab import axes as pyaxes
from pylab import plot as pyplot
from pylab import contourf as pycontourf
from mpl_toolkits.mplot3d import Axes3D

from pylab import title as pytitle
import matplotlib.pylab as pl
import matplotlib.gridspec as gridspec

from matplotlib.ticker import ScalarFormatter, FormatStrFormatter

     

class FixedOrderFormatter(ScalarFormatter):
    """Formats axis ticks using scientific notation with a constant order of 
    magnitude"""
    def __init__(self, order_of_mag=0, useOffset=True, useMathText=False):
        self._order_of_mag = order_of_mag
        ScalarFormatter.__init__(self, useOffset=useOffset, 
                                 useMathText=useMathText)
    def _set_orderOfMagnitude(self, range):
        """Over-riding this to avoid having orderOfMagnitude reset elsewhere"""
        self.orderOfMagnitude = self._order_of_mag

if(True):
      
      from matplotlib.collections import Collection
      from matplotlib.artist import allow_rasterization

      class ListCollection(Collection):
        def __init__(self, collections, **kwargs):
         Collection.__init__(self, **kwargs)
         self.set_collections(collections)
        def set_collections(self, collections):
         self._collections = collections
        def get_collections(self):
         return self._collections
        @allow_rasterization
        def draw(self, renderer):
         for _c in self._collections:
             _c.draw(renderer)

      def insert_rasterized_contour_plot(c):
        collections = c.collections
        for _c in collections:
          _c.remove()
        cc = ListCollection(collections, rasterized=True)
        ax = plt.gca()
        ax.add_artist(cc)
        return cc   

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 10}
matplotlib.rcParams['xtick.labelsize'] = 8
matplotlib.rcParams['ytick.labelsize'] = 8

matplotlib.rc('font', **font)

abohr = 0.529177249
hartree = 27.2114
lambdat = 0.01
pt_omega = 0.465298078279
pt_omega_ev = hartree*pt_omega
aulengthSI = 5.2917721092e-11
lengthboxau = pow((4*np.pi/pow(lambdat,2)),Fraction('1/3'))
coupling = pow(2.4403309e-12/(4.134137174e16),Fraction('1/2'))*1/pow(pt_omega,Fraction('1/2'))*pow(1/(aulengthSI*lengthboxau),Fraction('3/2'))
css = 1.25e-3
dimsize = 29
maxcolor = +1*1e-8
mincolor = maxcolor # this is the absolut value of the scale
outfile='bondcorrelation_resminushalfres.pdf'

def main():

    lenmax = 888 #1100 #888
    data_00019 = np.zeros((lenmax,5))
    data_00039 = np.zeros((lenmax,5))
    data_0039 = np.zeros((lenmax,5))

    data_00019 = data_00019 + np.loadtxt('0.000195/05/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/12/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/14/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/18/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/20/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/22/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/28/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 + np.loadtxt('0.000195/29/bond_correlation.txt')[:lenmax,:]
    data_00019 = data_00019 / 8.0

    data_00039 = data_00039 + np.loadtxt('0.00039/05/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/12/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/14/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/18/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/20/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/22/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/28/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 + np.loadtxt('0.00039/29/bond_correlation.txt')[:lenmax,:]
    data_00039 = data_00039 / 8.0

    data_0039 = data_0039 + np.loadtxt('0.0039/05/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/12/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/14/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/18/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/20/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/22/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/28/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 + np.loadtxt('0.0039/29/bond_correlation.txt')[:lenmax,:]
    data_0039 = data_0039 / 8.0

##############################
    low = 240
    up = 640
    XX = data_0039[low:up,0]
    #average_dis = np.zeros(7)
    #average_var = np.zeros(7)

    #fig, (ax0, ax3, ax4) = plt.subplots(nrows=1,ncols=3,sharex=True)
    gs = gridspec.GridSpec(4,3)
    gs.update(left=0.05, right=0.95, wspace=0.3, hspace=0.25)
    pl.figure(figsize=(14, 10))
    ax0 = pl.subplot(gs[0,:3]) # row 0, col 0
    ax0.plot(XX,(data_0039[low:up,1]-data_00019[low:up,1]),label=r'$|<$F-Si$|$Si-C$>|$')
    ax0.plot(XX,(data_0039[low:up,2]-data_00019[low:up,2]),label=r'$|<$Si-C$|$C=C$>|$')
    ax0.plot(XX,(data_0039[low:up,3]-data_00019[low:up,3]),label=r'$|<$C=C$|$C-B$>|$')
    ax0.plot(XX,(data_0039[low:up,4]-data_00019[low:up,4]),label=r'$|<$Si-C$|$C-B$>|$')
    ax0.legend(frameon=False)
    ax0.set(xlabel=r'Time (fs)', ylabel=r'$f(\omega_r)-f(0.05\omega_r)$')
    #ax0.set_xlim(200,500)

    integrated_0039 = [np.sum(data_0039[low:up,1])*(XX[1]-XX[0]),np.sum(data_0039[low:up,2])*(XX[1]-XX[0]),np.sum(data_0039[low:up,3])*(XX[1]-XX[0]),np.sum(data_0039[low:up,4])*(XX[1]-XX[0])]
    integrated_00019 = [np.sum(data_00019[low:up,1])*(XX[1]-XX[0]),np.sum(data_00019[low:up,2])*(XX[1]-XX[0]),np.sum(data_00019[low:up,3])*(XX[1]-XX[0]),np.sum(data_00019[low:up,4])*(XX[1]-XX[0])]
    integrated_00039 = [np.sum(data_00039[low:up,1])*(XX[1]-XX[0]),np.sum(data_00039[low:up,2])*(XX[1]-XX[0]),np.sum(data_00039[low:up,3])*(XX[1]-XX[0]),np.sum(data_00039[low:up,4])*(XX[1]-XX[0])]

    ax1 = pl.subplot(gs[1:,2]) # row 0, col 0
    ax1.plot(integrated_0039,'ro',label=r'$\omega_c=\omega_r$')
    ax1.plot(integrated_00039,'yd',label=r'$\omega_c=0.1\omega_r$')
    ax1.plot(integrated_00019,'bx',label=r'$\omega_c=0.05\omega_r$')
    ax1.legend(frameon=False)
    ax1.set(xlabel=r'$|<$F-Si$|$Si-C$>|$; $|<$Si-C$|$C=C$>|$; $|<$C=C$|$C-B$>|$; $|<$Si-C$|$C-B$>|$', ylabel=r'Integrated force-projections')

    ax0 = pl.subplot(gs[1,:2]) # row 0, col 0
    ax0.plot(XX,(data_0039[low:up,1]),label=r'$|<$F-Si$|$Si-C$>|$')
    ax0.plot(XX,(data_0039[low:up,2]),label=r'$|<$Si-C$|$C=C$>|$')
    ax0.plot(XX,(data_0039[low:up,3]),label=r'$|<$C=C$|$C-B$>|$')
    ax0.plot(XX,(data_0039[low:up,4]),label=r'$|<$Si-C$|$C-B$>|$')
    ax0.legend(frameon=False)
    ax0.set(xlabel=r'Time (fs)', ylabel=r'Force-projections $\omega_c=\omega_r$')
    #ax0.set_xlim(200,500)

    ax0 = pl.subplot(gs[3,:2]) # row 0, col 0
    ax0.plot(XX,(data_00019[low:up,1]),label=r'$|<$F-Si$|$Si-C$>|$')
    ax0.plot(XX,(data_00019[low:up,2]),label=r'$|<$Si-C$|$C=C$>|$')
    ax0.plot(XX,(data_00019[low:up,3]),label=r'$|<$C=C$|$C-B$>|$')
    ax0.plot(XX,(data_00019[low:up,4]),label=r'$|<$Si-C$|$C-B$>|$')
    ax0.legend(frameon=False)
    ax0.set(xlabel=r'Time (fs)', ylabel=r'Force-projections $\omega_c=0.05\omega_r$')
    #ax0.set_xlim(200,500)

    ax0 = pl.subplot(gs[2,:2]) # row 0, col 0
    ax0.plot(XX,(data_00039[low:up,1]),label=r'$|<$F-Si$|$Si-C$>|$')
    ax0.plot(XX,(data_00039[low:up,2]),label=r'$|<$Si-C$|$C=C$>|$')
    ax0.plot(XX,(data_00039[low:up,3]),label=r'$|<$C=C$|$C-B$>|$')
    ax0.plot(XX,(data_00039[low:up,4]),label=r'$|<$Si-C$|$C-B$>|$')
    ax0.legend(frameon=False, loc='upper right')
    ax0.set(xlabel=r'Time (fs)', ylabel=r'Force-projections $\omega_c=0.1\omega_r$')
    #ax0.set_xlim(200,500)

plt.show()
    ##############


    #return

##############################

    
    

main()
plt.savefig(outfile,dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)


