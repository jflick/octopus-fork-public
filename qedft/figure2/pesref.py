#!/usr/bin/python                                                                                                                                             

import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
from matplotlib.lines import Line2D

## you want to see the projected forces?
plotforces = False # it seems as if something is off with the forces, not sure what

np.set_printoptions(threshold=1e20)
font = {'family' : 'serif',
        'color'  : 'black',
        'weight' : 'normal',
        'size'   : 20,
        }
mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rc('font', family='serif')
mpl.rc('font', serif='Times')
mpl.rcParams.update({'font.size': 20})

data = np.loadtxt('pes.txt')
data[:,2] -= min(data[:,2])
#xi = np.arange(1.88572,3.82572,0.02)
#yi = np.arange(1.5345,3.3145,0.02)

xi = np.arange(1.70572,3.82572,0.01)
yi = np.arange(1.4345,3.5345,0.01)

Z = griddata(data[:,0], data[:,1], data[:,2], xi, yi, interp='linear')
X, Y = meshgrid(xi,yi)
Z *= 27.211
figure = plt.figure(figsize=(12,11))

##Contour
ax1 = figure.add_subplot(111)
N=100
CP = ax1.contourf(X, Y, Z, N, cmap='Blues_r')
cbar = figure.colorbar(CP, ax=ax1)
cbar.set_label('Energy (eV)')
levels = [i for i in np.arange(0,2.4,0.2)]
CS = ax1.contour(X, Y, Z, levels, colors='w', linewidths=2)
ax1.clabel(CS, inline=1, fontsize=11)

## Min and Transitionstate
sic_tr = np.sqrt( (-4.31868035794688--1.31945952132308)**2 + (-0.67348695240951--0.58848073765903)**2 + (-0.01189636293482-0.05491412880718)**2 )
sif_tr = np.sqrt( (-4.31868035794688--6.00192789163783)**2 + (-0.67348695240951--0.72503496479169)**2 + (-0.01189636293482--0.05707493075872)**2 )
sic_min = np.sqrt( (-3.75043365562725--1.66514419698593)**2 + (-0.61636552417923--0.65843215631630)**2 + (0.02234105316658-0.01732935535866)**2 )
sif_min = np.sqrt( (-3.75043365562725--5.48452006009059)**2 + (-0.61636552417923--0.58029901284460)**2 + (0.02234105316658-0.01094627968595)**2 )

## load trajectory
geo_data = np.loadtxt('td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data = np.loadtxt('td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_noc = np.loadtxt('../05_p10/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_noc = np.loadtxt('../05_p10/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
geo_data_offr = np.loadtxt('../../300K_cavity_0.007800445977268/05_p10_coupling4-24hqueue/td.general/coordinates', skiprows=6, usecols=(0,1 ,2,3,4, 5,6,7, 8,9,10)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position
forces_data_offr = np.loadtxt('../../300K_cavity_0.007800445977268/05_p10_coupling4-24hqueue/td.general/coordinates', skiprows=6, usecols=(0,1 ,2+27*3*2,3+27*3*2,4+27*3*2, 5+27*3*2,6+27*3*2,7+27*3*2, 8+27*3*2,9+27*3*2,10+27*3*2)) # 0 step, 1 time, 2-4 F position, 5-7 Si position, 8-10 C position

## loop over trajectory and plot configuration (+force)
counter = 0
for i in range(int(np.floor(geo_data[-1,0]*0.01))):
   counter += 1

   Si_coord_noc = geo_data_noc[i*100,5:7]
   C_coord_noc = geo_data_noc[i*100,8:10]
   F_coord_noc = geo_data_noc[i*100,2:4]
   SiF_dist_noc = np.sqrt(np.sum(pow(F_coord_noc - Si_coord_noc,2)))*0.529177249
   SiC_dist_noc = np.sqrt(np.sum(pow(Si_coord_noc - C_coord_noc,2)))*0.529177249
   ax1.plot(SiC_dist_noc, SiF_dist_noc, marker='o', color='black', markersize=4, alpha=abs(C_coord_noc - Si_coord_noc)[0]*0.529177249/SiC_dist_noc,label=r'$g_0/\omega=1.132$')

   Si_coord_offr = geo_data_offr[i*100,5:7]
   C_coord_offr = geo_data_offr[i*100,8:10]
   F_coord_offr = geo_data_offr[i*100,2:4]
   SiF_dist_offr = np.sqrt(np.sum(pow(F_coord_offr - Si_coord_offr,2)))*0.529177249
   SiC_dist_offr = np.sqrt(np.sum(pow(Si_coord_offr - C_coord_offr,2)))*0.529177249
   ax1.plot(SiC_dist_offr, SiF_dist_offr, marker='o', color='red', markersize=4, alpha=abs(C_coord_offr - Si_coord_offr)[0]*0.529177249/SiC_dist_offr,label=r'$g_0/\omega=1.132$')

   Si_coord = geo_data[i*100,5:7]
   C_coord = geo_data[i*100,8:10]
   F_coord = geo_data[i*100,2:4]
   SiF_dist = np.sqrt(np.sum(pow(F_coord - Si_coord,2)))*0.529177249
   SiC_dist = np.sqrt(np.sum(pow(Si_coord - C_coord,2)))*0.529177249
   ax1.plot(SiC_dist, SiF_dist, marker='o', color='orange', markersize=4, alpha=abs(C_coord - Si_coord)[0]*0.529177249/SiC_dist,label=r'$g_0/\omega=1.132$')

ax1.plot(sic_tr,sif_tr,marker='*',color='yellow', markersize=25, markeredgewidth=1.0, markeredgecolor="k")
#ax1.plot(sic_min,sif_min, 'yo', markersize=15)

plt.xlabel('Si-C distance ($\AA$)')#, fontdict=font)
plt.ylabel('Si-F distance ($\AA$)')#, fontdict=font)
plt.xlim(1.70572,3.05)
plt.ylim(1.4345,3.5)
plt.locator_params(axis='x', nbins=5)
plt.locator_params(axis='y', nbins=6)
plt.tick_params(axis='x', pad=10)
#plt.legend(loc='upper right')

figure.savefig('frames/trajectory_pes_combined.png',dpi=600, orientation='portrait',bbox_inches='tight', pad_inches=0.01)

plt.close()

